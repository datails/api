# React-admin NestJS Portal
React Admin portal to manage and configure reservations and properties.

## Use cases
This portal is built to easily manage properties for property owners and rent them on a front-end.

## Technologies
* [React Admin](https://marmelab.com/react-admin/) for the admin portal
* React for the front-end application
* [NestJS/MongoDB](https://nestjs.com/) for the API

## Start app
```bash
docker compose up
```

App exposed on port `9998`.

## Admin portal
React Admin portal visible at `/admin`. Please make sure you have some users created.

### Create an initial user
If no users created, create one via `/admin` > create user.

### Working of admin portal
The admin portal has 2 roles: superuser and admin. If you are an admin you own a portal where you can manage your own accomodations, reservations etcetera. Each accomodation you create, and is put to isActive will be shown on the front-end. The superuser can manage all portals.

## Frontend
React Front-end exposed on `/`.

## Backend
NestJS backend exposed on `/api`.