import React from "react";
import App from "./router";
import AppLoader from "./framework/loader";

export default function AppMain() {
  return (
    <React.Fragment>
      <AppLoader />
      <App />
    </React.Fragment>
  );
}
