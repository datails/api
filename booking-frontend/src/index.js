import "dotenv/config";
import React from "react";
import ReactDOM from "react-dom";
import AppMain from "./app-main";
import Store from "./store/store";
import "./index.css";
import { getRootElement } from "./utils";

ReactDOM.render(
  <Store>
    <AppMain />
  </Store>,
  getRootElement()
);
