import * as R from 'ramda';
import { stringify } from "query-string";
import * as U from "./utils";

export const request = R.curryN(2, (
  portalId,
  path
) => {
  return fetch(path, {
    headers: {
      'x-portal-id': portalId
    }
  })
});

export const httpGet = async path => {
  const portalId = window.sessionStorage.getItem('portalId');
  const res = await request(portalId, `${process.env.REACT_APP_API_HOST || ''}${path}`)

  if (!res.ok) {
    return JSON.parse((await res.text()))
  }

  const { data } = await res.json()
    .catch(console.error);

  return data;
}

export const loadReservation = id => httpGet(`/api/v1/public/reservation/${id}`);
export const loadProperty = ({ id }) => httpGet(`/api/v1/public/property/${id}`);
export const loadPortal = id => httpGet(`/api/v1/public/portal/${id}`);
export const loadProperties = () => httpGet('/api/v1/public/property-list');
export const loadSchemas = () => httpGet("/api/v1/public/property-schema");

export const loadOwner = async (owner) => {
  const data = await httpGet(`/api/v1/public/owner/${owner}`);

  return {
    ...data,
    file: {
      ...data.file,
      filename: U.getRefToFile(data.file),
    },
  };
}

export async function loadAvailability({ startDate = new Date(), id }) {
  const query = U.getQuery(startDate, id);
  const data = await httpGet(`/api/v1/public/availability?${stringify(query)}`);
  const blockedDays = U.getBlockedDays(data);

  return {
    data,
    blockedDays,
  };
}

export async function loadReservations({ startDate = new Date(), id }) {
  const query = U.getQuery(startDate, id);
  const data = await httpGet(`/api/v1/public/reservations?${stringify(query)}`);
  const blockedDays = U.getBlockedDays(data);

  return {
    blockedDays,
    data: data,
  };
}

export async function loadPropertyWithReservations({
  id,
  startDate = new Date(),
}) {
  const property = await loadProperty({ id });
  const portal = await loadPortal(property.portalId);
  const owner = await loadOwner(portal.ownerId);

  const [reservations, availability] = await Promise.all([
    loadReservations({
      id,
      startDate,
    }),
    loadAvailability({
      id,
      startDate,
    }),
  ]);

  return {
    ...property,
    blockedDays: [...reservations?.blockedDays, ...availability?.blockedDays],
    owner,
    reservations,
    availability,
  };
}

export async function createReservation({
  reservation,
  recaptchaValue,
  client,
}) {
  const price = U.getTotalPriceOfProperty(
    reservation.property.price,
    U.parseDateOrString(reservation.startDate).toISOString(),
    U.parseDateOrString(reservation.endDate).toISOString(),
    reservation.property.availability.data,
    reservation.property.minNights
  );

  const res = await fetch(`${process.env.REACT_APP_API_HOST || ''}/api/v1/public/create-reservation`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      'x-portal-id': window.sessionStorage.getItem('portalId')
    },
    body: JSON.stringify({
      client: {
        ...client,
        housenumber: parseInt(client.housenumber),
      },
      reservation: {
        ...reservation,
        property: reservation.property.id,
        price,
      },
      recaptchaValue,
    }),
  });

  if (!res.ok) {
    return JSON.parse((await res.text()))
  }

  const { data } = await res.json();

  return data;
}

export async function createClientAndReservation({
  client,
  reservation,
  recaptchaValue,
}) {
  const reservationResp = await createReservation({
    reservation: {
      ...reservation,
      recaptchaValue,
    },
    recaptchaValue,
    client,
  })

  return {
    client: reservationResp?.client,
    reservation: reservationResp?.reservation,
    error: (Boolean(reservationResp?.message) && Boolean(reservationResp?.statusCode)),
    errorMessage: reservationResp?.message
  };
}
