import React, { useContext } from "react";
import classNames from "classnames";
import {
  FormGroup,
  FormControlLabel,
  Checkbox,
  Paper,
  Typography,
} from "@material-ui/core";
import StickyBox from "react-sticky-box";

import { AppAccordion } from "../../../../components";
import withRoot from "../../../../theme/withRoot";

import { translate } from "../../app-catalogue.content";
import useStyles from "./app-catalogue-filter.styles";

function AppCatalogueFilter({ schemas, services, setServices }) {
  const classes = useStyles();

  return (
    <StickyBox offsetTop={80} offsetBottom={20}>
      <Paper className={classes.sideSearch} id="search-container">
        <Typography
          gutterBottom
          variant="h5"
          component="h2"
          className={classes.sideBarTitle}
        >
          Verfijn zoekopdracht
        </Typography>
        <FormGroup row>
          {Object.keys(schemas).map((key, index) => {
            if (key.startsWith("has") && index < 5) {
              return (
                <FormControlLabel
                  className={classes.formControl}
                  control={
                    <Checkbox
                      checked={services[key]}
                      onChange={setServices}
                      name={key}
                    />
                  }
                  label={translate[key]}
                />
              );
            }
            return null;
          })}
          <AppAccordion>
            {Object.keys(schemas).map((key, index) => {
              if (key.startsWith("has") && index >= 5) {
                return (
                  <FormControlLabel
                    className={classNames(classes.formControl)}
                    control={
                      <Checkbox
                        checked={services[key]}
                        onChange={setServices}
                        name={key}
                      />
                    }
                    label={translate[key]}
                  />
                );
              }
              return null;
            })}
          </AppAccordion>
        </FormGroup>
      </Paper>
    </StickyBox>
  );
}

export default withRoot(AppCatalogueFilter);
