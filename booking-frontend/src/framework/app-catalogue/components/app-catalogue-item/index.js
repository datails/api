import React, { useContext } from "react";
import {
  Card,
  Grid,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import WifiIcon from "@material-ui/icons/Wifi";
import PetsIcon from "@material-ui/icons/Pets";
import AccessibleIcon from "@material-ui/icons/Accessible";
import PeopleIcon from "@material-ui/icons/People";
import classnames from "classnames";
import { differenceInCalendarDays } from "date-fns";

import useStyles from "./app-catalogue-item.styles";

import { Context } from "../../../../store/store";
import { parseDateOrString, getRefToFile } from "../../../../utils";

export default function AppCatalogueListItem({ item }) {
  const classes = useStyles();
  const [state] = useContext(Context);

  const checkout = parseDateOrString(state.search.checkout);
  const checkin = parseDateOrString(state.search.checkin);
  const days = differenceInCalendarDays(checkout, checkin);
  const img = `${process.env.REACT_APP_API_HOST}${item.images.find(Boolean)}`;

  return (
    <Card className={classes.card}>
      <Grid
        container
        justify="space-around"
        className={classes.container}
        spacing={10}
      >
        <Grid item className={classes.content}>
          <CardMedia
            component="img"
            alt={item.title}
            height="140"
            image={img}
            title={item.title}
            className={classes.media}
          />
          <Grid
            container
            justify="space-around"
            className={classes.container}
            spacing={10}
          >
            <Grid item xs={12} className={classes.content}>
              <CardContent
                className={classnames(
                  classes.content,
                  classes.padding,
                  classes.flexDirectionColumn
                )}
              >
                <Typography
                  gutterBottom
                  variant="h5"
                  component="h2"
                  className={classes.title}
                >
                  {item.title}
                </Typography>
                <Typography
                  variant="body2"
                  color="textSecondary"
                  component="p"
                  color="primary"
                >
                  {item.city}
                </Typography>
                <Typography
                  variant="body2"
                  color="textSecondary"
                  component="p"
                  className={classes.vertAlign}
                  color="secondary"
                >
                  {item.hasWifi && <WifiIcon className={classes.smallIcons} />}
                  {item.isFitForAnimals && (
                    <PetsIcon className={classes.smallIcons} />
                  )}
                  {item.isHandicapped && (
                    <AccessibleIcon className={classes.smallIcons} />
                  )}
                  {<PeopleIcon className={classes.smallIcons} />}
                  <b className={classes.smallIcons}>{item.persons}</b> personen
                  - <b className={classes.smallIcons}>{item.beds}</b> bedden
                </Typography>
              </CardContent>
            </Grid>
            <Grid
              item
              xs={12}
              className={classnames(classes.content, classes.backgroundLight)}
            >
              <CardActions className={classes.cardActions}>
                <Typography
                  gutterBottom
                  variant="body"
                  component="p"
                  className={classes.vertAlign}
                >
                  <span className={classes.cta}>{days}</span> dagen -{" "}
                  {new Intl.NumberFormat("nl-NL", {
                    style: "currency",
                    currency: "EUR",
                  }).format(days * item.price)}
                </Typography>
                <Typography
                  gutterBottom
                  variant="body2"
                  component="p"
                  // color="secondary"
                >
                  <b>Per nacht</b>
                  <br />
                  <b>
                    {new Intl.NumberFormat("nl-NL", {
                      style: "currency",
                      currency: "EUR",
                    }).format(item.price)}
                  </b>
                </Typography>
                <Button
                  className={classes.button}
                  href={`/vakantiehuisjes/${item._id}`}
                  color="primary"
                >
                  Bekijk
                </Button>
              </CardActions>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Card>
  );
}
