import React, { useState } from "react";
import { Grid, Paper } from "@material-ui/core";

import { AppCirculairLoader, AppSearch } from "../../components";
import withRoot from "../../theme/withRoot";

import { AppCatalogueList, AppCatalogueFilter } from "./components";
import useStyles from "./app-catalogue.styles";
import PropertiesSwitcher from "./app-catalogue.switch";

import { SchemaContext, PropertyContext } from "./app-catalogue.context";

function AppCatalogue() {
  const classes = useStyles();
  const [schemas, setSchemas] = useState();
  const [properties, setProperties] = useState();
  const [services, setServices] = useState({});

  const handleServiceChange = (event) => {
    setServices({ ...services, [event.target.name]: event.target.checked });
  };

  if (!schemas || !properties)
    return (
      <PropertyContext.Provider value={{ properties, setProperties }}>
        <SchemaContext.Provider value={{ schemas, setSchemas }}>
          <PropertiesSwitcher />
        </SchemaContext.Provider>
      </PropertyContext.Provider>
    );

  return (
    <Grid
      container
      justify="space-around"
      className={classes.container}
      spacing={10}
    >
      <Grid item sm={12} md={4} className={classes.content}>
        <AppCatalogueFilter
          schemas={schemas}
          services={services}
          setServices={handleServiceChange}
        />
      </Grid>
      <Grid item sm={12} md={8} className={classes.content}>
        <Paper className={classes.searchBar}>
          <AppSearch showSearch={false} />
        </Paper>
        {!properties ? (
          <AppCirculairLoader />
        ) : (
          <AppCatalogueList properties={properties} services={services} />
        )}
      </Grid>
    </Grid>
  );
}

export default withRoot(AppCatalogue);
