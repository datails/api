import React from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) =>
  createStyles({
    button: {
      color: "#FFF",
      fontSize: "1.1rem",
      height: "50px",
      margin: theme.spacing(1),
      minWidth: "175px",
      backgroundColor: theme.palette.primary.main,
      "&:hover": {
        backgroundColor: theme.palette.primary.dark,
      },
    },
  })
);

export default function CustomizedButtons({
  props,
  children,
  styles,
  href,
  disabled = false,
  handler,
}) {
  const classes = useStyles();

  return (
    <Button
      disabled={disabled}
      onClick={handler}
      href={href}
      variant="contained"
      // color="primary"
      style={styles || {}}
      className={classes.button}
      {...props}
    >
      {children}
    </Button>
  );
}
