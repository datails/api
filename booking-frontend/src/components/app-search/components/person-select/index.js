import React, { useContext } from "react";
import { Context } from "../../../../store/store";
import AppFab from "../fab";

export default function PersonSelectors() {
  const [state, dispatch] = useContext(Context);

  const setState = (key, value) => {
    dispatch({
      type: "SET_SEARCH",
      search: {
        [key]: value,
      },
    });
  };

  return (
    <>
      <AppFab
        state={state.search.adults}
        setState={(e) => setState("adults", e)}
        name={"Volwassen"}
        desc={"Personen vanaf 18 jaar"}
      />
      <AppFab
        state={state.search.children}
        setState={(e) => setState("children", e)}
        name={"Kinderen"}
        desc={"3 t/m 17 jaar"}
      />
      <AppFab
        state={state.search.babies}
        setState={(e) => setState("babies", e)}
        name={"Baby's"}
        desc={"0 t/m 2 jaar"}
      />
    </>
  );
}
