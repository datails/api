import "date-fns";
import React, { useContext } from "react";
import { Grid } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import DateFnsUtils from "@date-io/date-fns";
import dutchLocale from "date-fns/locale/nl";
import { addDays } from "date-fns";
import { MuiPickersUtilsProvider, DatePicker } from "@material-ui/pickers";
import { navigate } from "hookrouter";
import AppButton from "../app-button";
import { AppPersons } from "./components";
import { Context } from "../../store/store";
import { parseDateOrString } from "../../utils";
import useStyles from "./app-search.styles";
import withRoot from "../../theme/withRoot";

function AppSearch({ showSearch = true, direction = "row" }) {
  const date = new Date();
  const classes = useStyles();
  const [state, dispatch] = useContext(Context);

  let checkin = parseDateOrString(state.search.checkin) || new Date();
  let checkout = parseDateOrString(state.search.checkout) || new Date();

  if (checkin < date) {
    dispatch({
      type: "SET_SEARCH",
      search: {
        checkin: addDays(date, 1).toISOString(),
        checkout: addDays(date, 2).toISOString(),
      },
    });
  }

  if (checkout < checkin) {
    dispatch({
      type: "SET_SEARCH",
      search: {
        checkout: addDays(checkin, 1).toISOString(),
      },
    });
  }

  const handleSearch = (search) => {
    const parsedSearch = JSON.parse(JSON.stringify(search));

    dispatch({
      type: "SET_SEARCH",
      search: parsedSearch,
    });

    navigate("/vakantiehuisjes", false, {
      ...state.search,
      ...parsedSearch,
      checkin: parsedSearch.checkin.split("T")[0],
      checkout: parsedSearch.checkout.split("T")[0],
    });
  };

  const handleCheckInChange = (date) => {
    dispatch({
      type: "SET_SEARCH",
      search: {
        checkin: date,
      },
    });
  };

  const handleCheckoutChange = (date) => {
    dispatch({
      type: "SET_SEARCH",
      search: {
        checkout: date,
      },
    });
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={dutchLocale}>
      <Grid
        container
        justify="space-between"
        direction={direction}
        className={classes.container}
        spacing={10}
        component={"nav"}
      >
        <Grid className={classes.content}>
          <DatePicker
            minDate={date}
            margin="normal"
            id="startDate"
            label="Inchecken"
            format="MM/dd/yyyy"
            value={checkin || date}
            onChange={handleCheckInChange}
            title={"Inchecken"}
            cancelLabel={"Annuleren"}
            className={classes.datePicker}
          />
          {direction === "row" && <div className={classes.borderRight}></div>}
        </Grid>
        <Grid className={classes.content}>
          {checkin && (
            <DatePicker
              minDate={checkin || date}
              margin="normal"
              id="endDate"
              label="Uitchecken"
              format="MM/dd/yyyy"
              value={checkout || date}
              onChange={handleCheckoutChange}
              title={"Uitchecken"}
              cancelLabel={"Annuleren"}
              className={classes.datePicker}
            />
          )}
          {direction === "row" && <div className={classes.borderRight}></div>}
        </Grid>
        <Grid className={classes.content}>
          <AppPersons className={classes.datePicker} />
        </Grid>
        {showSearch && (
          <>
            <Grid className={classes.content}>
              {state.search.adults > 0 ? (
                <AppButton
                  handler={() =>
                    handleSearch({
                      checkin: state?.search?.checkin || date,
                      checkout: state?.search?.checkout || date,
                    })
                  }
                  styles={{
                    borderRadius: "100%",
                    width: "50px",
                    height: "50px",
                    minWidth: "50px",
                    backgroundColor: "#095e4b",
                  }}
                >
                  <SearchIcon />
                </AppButton>
              ) : (
                <AppButton
                  disabled
                  handler={() =>
                    handleSearch({
                      checkin: state?.search?.checkin || date,
                      checkout: state?.search?.checkout || date,
                    })
                  }
                  styles={{
                    borderRadius: "100%",
                    width: "50px",
                    height: "50px",
                    minWidth: "50px",
                  }}
                >
                  <SearchIcon />
                </AppButton>
              )}
            </Grid>
          </>
        )}
      </Grid>
    </MuiPickersUtilsProvider>
  );
}

export default withRoot(AppSearch);
