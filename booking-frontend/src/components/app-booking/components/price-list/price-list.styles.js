import { makeStyles } from "@material-ui/core";

export default makeStyles(() => ({
  content: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 30,
    width: "100%",
  },
  marginTop: {
    marginTop: 20,
  },
}));
