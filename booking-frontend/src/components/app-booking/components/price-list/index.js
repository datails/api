import React from "react";
import { Divider } from "@material-ui/core";
import { isBefore } from "date-fns";
import {
  getTotalPriceOfProperty,
  getDifferenceInDays,
  parseDateOrString,
} from "../../../../utils";

import withRoot from "../../../../theme/withRoot";

import useStyles from "./price-list.styles";

function AppPriceList({ property, checkin, checkout }) {
  const classes = useStyles();

  checkin = parseDateOrString(checkin);
  checkout = parseDateOrString(checkout);

  const taxPerNight = 1.35;

  const days = getDifferenceInDays(
    checkin.toISOString(),
    checkout.toISOString(),
    property.minNights
  );

  const totalTaxPrice = days * taxPerNight;

  const totalPrice = isBefore(checkin, checkout)
    ? getTotalPriceOfProperty(
        property.price,
        checkin.toISOString(),
        checkout.toISOString(),
        property.availability.data,
        property.minNights
      )
    : 0;

  return (
    <React.Fragment>
      <div className={classes.content}>
        <span>
          Prijs{" "}
          {property.isVATIncluded ? (
            <i>(Inclusief BTW 21%)</i>
          ) : (
            <i>(Exc. BTW)</i>
          )}
        </span>
        <span>
          {new Intl.NumberFormat("nl-NL", {
            style: "currency",
            currency: "EUR",
          }).format(totalPrice)}
        </span>
      </div>
      <div className={classes.content}>
        <span>Servicekosten</span>
        <span>
          {new Intl.NumberFormat("nl-NL", {
            style: "currency",
            currency: "EUR",
          }).format(property.servicePrice)}
        </span>
      </div>
      <div className={classes.content}>
        <span>Toeristenbelasting</span>
        <span>
          {new Intl.NumberFormat("nl-NL", {
            style: "currency",
            currency: "EUR",
          }).format(totalTaxPrice)}
        </span>
      </div>
      <Divider className={classes.marginTop} />
      <div className={classes.content}>
        <b>Totaal</b>
        <b>
          {new Intl.NumberFormat("nl-NL", {
            style: "currency",
            currency: "EUR",
          }).format(totalPrice + property.servicePrice + totalTaxPrice)}
        </b>
      </div>
    </React.Fragment>
  );
}

export default withRoot(AppPriceList);
