import React, { useContext } from "react";
import { navigate } from "hookrouter";
import { AppButton } from "../../..";
import { Context } from "../../../../store/store";
import withRoot from "../../../../theme/withRoot";
import useStyles from "./reservation-button.styles";

function AppBookButton({ property }) {
  const [state, dispatch] = useContext(Context);
  const classes = useStyles();
  const isButtonDisabled =
    state.search.adults <= 0 || !state.search.checkout || !state.search.checkin;

  const handleReservation = () => {
    const parsedProperty = JSON.parse(JSON.stringify(property));

    dispatch({
      type: "SET_PROPERTY",
      property: {
        ...parsedProperty,
      },
    });

    navigate(`/vakantiehuisjes/${property.id}/reserveren`, false);
  };

  if (isButtonDisabled) {
    return (
      <AppButton
        disabled
        handler={() => handleReservation()}
        styles={{
          background: classes.button,
        }}
      >
        Reserveren
      </AppButton>
    );
  }

  return (
    <AppButton
      handler={() => handleReservation()}
      styles={{
        background: classes.button,
      }}
    >
      Reserveren
    </AppButton>
  );
}

export default withRoot(AppBookButton);
