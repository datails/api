import React from "react";
import { makeStyles, createStyles } from "@material-ui/core";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import ArrawBackIosIcon from "@material-ui/icons/ArrowBackIos";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";

const useStyles = makeStyles((theme) =>
  createStyles({
    fullWidth: {
      width: "100%",
    },
    images: {
      background: "#FFF",
    },
    widthAuto: {
      margin: "auto",
    },
    arrowRight: {
      cursor: "pointer",
      position: "absolute",
      top: "40%",
      bottom: "auto",
      padding: ".4em",
      zIndex: 2,
      right: "4.3em",
      color: "#FFF",
    },
    arrowLeft: {
      cursor: "pointer",
      position: "absolute",
      top: "40%",
      bottom: "auto",
      padding: ".4em",
      zIndex: 2,
      left: "4.3em",
      color: "#FFF",
    },
  })
);

function getRefToFile(file) {
  const [name, ext] = file.filename?.split?.(".") || [];
  return `${process.env.REACT_APP_API_HOST || ''}/api/v1/uploads/${name}-m.${ext}`;
}

export default function AppCarousel({ items }) {
  const classes = useStyles();
  const images = items.map((file) => getRefToFile(file));

  return (
    <Carousel
      showThumbs
      infiniteLoop
      showStatus={false}
      renderArrowNext={(onClickHandler, hasNext) =>
        hasNext && (
          <div onClick={onClickHandler} className={classes.arrowRight}>
            <ArrowForwardIosIcon />
          </div>
        )
      }
      renderArrowPrev={(onClickHandler, hasPrev) =>
        hasPrev && (
          <div onClick={onClickHandler} className={classes.arrowLeft}>
            <ArrawBackIosIcon />
          </div>
        )
      }
    >
      {images.map((image, id) => (
        <div key={id} className={classes.images}>
          <img src={image} className={classes.widthAuto} />
        </div>
      ))}
    </Carousel>
  );
}
