export default (state, action) => {
  let newState = {};

  switch (action.type) {
    case "SET_SEARCH":
      newState = {
        ...state,
        search: {
          ...state.search,
          ...action.search,
        },
      };
      break;

    case "SET_PROPERTY":
      newState = {
        ...state,
        property: action.property,
      };
      break;

    case "SET_CLIENT":
      newState = {
        ...state,
        client: action.client,
      };
      break;

    default:
      newState = state;
  }

  localStorage.setItem("root:state", JSON.stringify(newState));

  return newState;
};
