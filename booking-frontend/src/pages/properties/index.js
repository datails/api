import React from "react";
import withRoot from "../../theme/withRoot";
import { AppCatalogue } from "../../framework";

function AppVakantieHuisjes() {
  return (
      <AppCatalogue />
  );
}

export default withRoot(AppVakantieHuisjes);
