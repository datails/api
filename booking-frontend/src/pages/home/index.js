import React from "react";
import { Paper } from '@material-ui/core';
import withRoot from "../../theme/withRoot";
import Search from '../../components/app-search'


function AppHome() {
  return (
    <Paper>
      <Search />
    </Paper>
  );
}

export default withRoot(AppHome);
