import React, { useContext } from "react";
import { Paper, Typography } from "@material-ui/core";
import withRoot from "../../../../theme/withRoot";
import { Context } from "../../../../store/store";
import AppBooking from "../../../../components/app-booking";

import { content } from './property.content'
import useStyles from "./property-sidebar.styles"

function AppAvailabilitySidebar({ property }) {
  const classes = useStyles();
  const [state] = useContext(Context);
  const isValidBooking = (state.property && state.search.checkin && state.search.checkout);

  if (!isValidBooking || !property) return null;

  return (
    <Paper
      component="aside"
      className={classes.sideSearch}
      id="search-container"
    >
      <AppBooking initialProperty={property} readOnly />
      <br />
      <Typography gutterBottom variant="p" component="p">
        {content.details}
      </Typography>
    </Paper>
  );
}

export default withRoot(AppAvailabilitySidebar);
