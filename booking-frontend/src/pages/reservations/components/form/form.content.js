export const content = {
    validate: {
        invalidDate: 'U heeft geen datum doorgegeven wanneer u wilt komen!',
        invalidProperty: 'U heeft geen vakantiehuis geselecteerd!',
    },
    submitSuccess: "Reserveringsaanvraag is gelukt!"
}