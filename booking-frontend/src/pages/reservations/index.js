import React, { useState } from "react";
import { Grid } from "@material-ui/core";
import StickyBox from "react-sticky-box";

import withRoot from "../../theme/withRoot";
import { AppForm } from "./components";
import useStyles from "./reservations.styles";

import ReservationSidebar from "./components/property-sidebar";
import PropertyContext from "../property/property.context";
import PropertySwitcher from "../property/property.switch";

function AppReservations({ id }) {
  const classes = useStyles();
  const [property, setProperty] = useState();
  const value = { property, setProperty };

  if (!property) {
    return (
      <PropertyContext.Provider value={value}>
        <PropertySwitcher id={id} />
      </PropertyContext.Provider>
    );
  }

  return (
    <React.Fragment>
      <Grid
        container
        className={classes.container}
        justify="space-evenly"
        spacing={10}
      >
        <Grid item xs={12} md={8} justify="center" align="center">
          <AppForm />
        </Grid>
        <Grid item xs={12} md={4} justify="center" align="center">
          <StickyBox offsetTop={100} offsetBottom={20}>
            <ReservationSidebar property={property} />
          </StickyBox>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default withRoot(AppReservations);
