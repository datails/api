import { makeStyles, createStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    border: {
      width: "100%",
      // boxShadow: "-7px 7px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
      border: `1px solid ${theme.palette.primary.main}`,
      "&:hover": {
        boxShadow:
          "-8px 11px 13px 0px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
      },
    },
    container: {
      margin: "0 auto",
      width: "100%",
    },
    content: {
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      fontSize: "1.1rem",
      textAlign: "justify",
      fontWeight: 300,
      lineHeight: 1.8,
    },
    listItem: {
      paddingLeft: 0,
      fontSize: "20px",
    },
  })
);
