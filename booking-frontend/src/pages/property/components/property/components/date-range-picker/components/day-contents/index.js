import React from "react";
import moment from "moment";
import { parseISO } from "date-fns";
import useStyles from "./day-contents.styles";

import * as utils from "../../utils";
import withRoot from "../../../../../../../../theme/withRoot";

function DayContents({ day, property, availability }) {
  const classes = useStyles();
  const formattedDay = parseISO(moment(day).toISOString());

  return (
    <div className={classes.dayContentWrapper}>
      <span className={classes.dayContentPrice}>
        {new Intl.NumberFormat("nl-NL", {
          style: "currency",
          currency: "EUR",
        }).format(
          utils.getCorrectPrice(property.price, formattedDay, availability)
        )}
      </span>
      <span>{day.format("D")}</span>
    </div>
  );
}

export default withRoot(DayContents);
