import "moment/locale/nl";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import React, { useState, useContext } from "react";
import { Typography } from "@material-ui/core";
import { addDays, parseISO } from "date-fns";
import Moment from "moment";
import { extendMoment } from "moment-range";
import { DayPickerRangeController } from "react-dates";

import withRoot from "../../../../../../theme/withRoot";
import { Context } from "../../../../../../store/store";
import useStyles from "./date-range-picker.styles";
import { DayContents } from "./components";
import * as utils from "./utils";

const moment = extendMoment(Moment);

function AppDateRangePicker({ property }) {
  const classes = useStyles();
  const [state, dispatch] = useContext(Context);
  const [focusedInput, setFocusedInput] = useState("startDate");

  const priceSlots = property.availability?.data.filter?.(
    (prices) => prices.isAvailable
  );

  return (
    <>
      <Typography
        component="h2"
        variant="h4"
        gutterBottom
        className={classes.title}
      >
        Beschikbaarheid
      </Typography>
      <DayPickerRangeController
        navPosition="navPositionBottom"
        keepOpenOnDateSelect={true}
        minDate={moment()}
        minimumNights={property.minNights}
        startDate={moment(state?.search?.checkin || new Date())}
        endDate={moment(
          state?.search?.checkout ||
            addDays(
              parseISO(state?.search?.checkin || new Date()),
              property.numberOfDaysBeforeCancel
            )
        )}
        onDatesChange={({ startDate, endDate }) => {
          if (
            !utils.isDateBlocked(startDate, endDate, property.availability.data)
          ) {
            dispatch({
              type: "SET_SEARCH",
              search: {
                ...state.search,
                checkin: moment(startDate).toISOString(),
                checkout: moment(endDate).toISOString(),
              },
            });
          } else {
            dispatch({
              type: "SET_SEARCH",
              search: {
                ...state.search,
                checkin: moment(startDate).toISOString(),
              },
            });
          }
        }}
        focusedInput={focusedInput}
        onFocusChange={(fcsInput) => {
          setFocusedInput(fcsInput);
        }}
        renderDayContents={(day, modifiers) => {
          day._locale._weekdaysMin = ["ZO", "MA", "DI", "WO", "DO", "VR", "ZA"];
          return (
            <DayContents
              property={property}
              availability={priceSlots}
              day={day}
            />
          );
        }}
        numberOfMonths={1}
        isDayBlocked={(day) => {
          return utils.isDayBlocked({
            day,
            blockedDays: property.blockedDays,
            minNights: property.minNights,
          });
        }}
        orientation="horizontal"
        daySize={100}
        calendarInfoPosition="after"
        hideKeyboardShortcutsPanel
        noBorder
      />
    </>
  );
}

export default withRoot(AppDateRangePicker);
