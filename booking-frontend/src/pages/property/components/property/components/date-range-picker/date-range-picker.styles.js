import { makeStyles } from "@material-ui/core";

export default makeStyles(() => ({
  title: {
    marginTop: 30,
    marginBottom: 30,
    textAlign: "left",
  },
}));
