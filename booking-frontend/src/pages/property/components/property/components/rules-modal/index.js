import React from "react";
import Modal from "../../../../../../components/app-modal";

export default function ({ rules, handleOpen, handleClose, open, setOpen }) {
  return (
    <Modal
      title={"Huisregels"}
      html={rules}
      handleOpen={handleOpen}
      handleClose={handleClose}
      open={open}
      setOpen={setOpen}
    />
  );
}
