import React from "react";
import { Chip, Typography } from "@material-ui/core";
import DoneIcon from "@material-ui/icons/Done";
import { translate } from "../../../../../../framework/app-catalogue/app-catalogue.content";
import useStyles from "./provisions.styles";
import withRoot from "../../../../../../theme/withRoot";

function AppProvisions({ property }) {
  const classes = useStyles();
  const handler = () => {};

  return (
    <>
      <Typography
        component="h2"
        variant="h4"
        gutterBottom
        className={classes.title}
      >
        Voorzieningen
      </Typography>
      <div className={classes.root}>
        {Object.keys(property).map((key) => {
          if (key.startsWith("has") && property[key] === true) {
            return (
              <Chip
                color="secondary"
                label={translate[key]}
                deleteIcon={<DoneIcon />}
                onClick={handler}
                onDelete={handler}
              />
            );
          }
          return null;
        })}
      </div>
    </>
  );
}

export default withRoot(AppProvisions);
