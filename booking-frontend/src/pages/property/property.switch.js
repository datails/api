import React, { useContext, useEffect } from "react";
import PropertyContext from "./property.context";
import { loadPropertyWithReservations } from "../../utils/api";
import AppCirculairLoader from "../../components/app-loader";
import withRoot from "../../theme/withRoot";

function PropertySwitcher({ id }) {
  const { setProperty } = useContext(PropertyContext);

  const loadProperty = async () => {
    const resp = await loadPropertyWithReservations({ id });
    setProperty(resp);
  };

  useEffect(() => {
    loadProperty();
  }, []);

  return <AppCirculairLoader />;
}

export default withRoot(PropertySwitcher);
