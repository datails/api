import React, { useContext, useEffect } from "react";
import { BookingContext } from "./booking.context";
import { loadReservation } from "../../utils/api";
import AppCirculairLoader from "../../components/app-loader";
import withRoot from "../../theme/withRoot";

function BookingSwitch({ id }) {
  const { setReservation } = useContext(BookingContext);

  const loadBooking = async () => {
    const resp = await loadReservation(id);
    setReservation(resp);
  };

  useEffect(() => {
    loadBooking();
  }, []);

  return <AppCirculairLoader />;
}

export default withRoot(BookingSwitch);
