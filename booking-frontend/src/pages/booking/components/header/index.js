import React from "react";
import useHeaderStyles from "./header.styles";
import withRoot from "../../../../theme/withRoot";

function AppPropertyHeader() {
  const classes = useHeaderStyles();
  return <div className={classes.backgroundGreen}></div>;
}

export default withRoot(AppPropertyHeader);
