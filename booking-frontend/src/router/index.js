import React from "react";
import { useRoutes, useInterceptor } from "hookrouter";
import AppProperties from "../pages/properties";
import AppProperty from "../pages/property";
import AppReservations from "../pages/reservations";
import AppBooking from "../pages/booking";

const routes = {
  "/": () => <AppProperties />,
  "/vakantiehuisjes": () => <AppProperties />,
  "/vakantiehuisjes/:id": ({ id }) => <AppProperty id={id} />,
  "/vakantiehuisjes/:id/reserveren": ({ id }) => <AppReservations id={id} />,
  "/vakantiehuisjes/:id/boeking/:bookingId": ({ id, bookingId }) => <AppBooking id={id} bookingId={bookingId} />,
};

const App = () => {
  useInterceptor((_, nextPath) => {
    window.scrollTo(0, 0);
    return nextPath;
  });

  return useRoutes(routes);
};

export default App;
