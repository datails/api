export function getFileFromServer(file) {
  const [name, ext] = file?.filename?.split?.(".") || [];
  return `/api/v1/uploads/${name}-xs.${ext}`;
}

export function toOriginalFile(file) {
  return file?.filename?.replace("-xs.", ".").replace("/api/v1/uploads/", "");
}
