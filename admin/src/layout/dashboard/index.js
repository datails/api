import React, { useState } from "react";
import { useGetIdentity, useGetOne, Loading } from "react-admin";
import { Grid, Divider } from "@material-ui/core";
import withRoot from "../theme/withRoot";

// import PropertyContext from "./property.context";
// import PropertySwitcher from "./property.switch";
import useDashboardStyles from "./dashboard.styles";

const Dashboard = () => {
  const classes = useDashboardStyles();
  const { identity, loading: identityLoading } = useGetIdentity();
  const { data: portal, loading: portaslLoading } = useGetOne(
    "portals",
    identity?.portalId
  );
  const { data: layout, loading: layoutLoading } = useGetOne(
    "layouts",
    identity?.portalId
  );

  if (
    identityLoading ||
    portaslLoading ||
    layoutLoading ||
    !identity.portalId
  ) {
    return <Loading />;
  }

  return (
    <main>
      {/* <AppPropertyHeader />
      <AppBreadCrumb backgroundColor="#4092B8" current={property.title} />
      <Divider />
      <Grid
        container
        className={classes.container}
        justify="space-evenly"
        spacing={10}
      >
        <Grid item xs={12} md={8} justify="center" align="center">
          <AppProperty property={property} />
        </Grid>
        <Grid item xs={12} md={4} justify="center" align="center">
          <StickyBox offsetTop={100} offsetBottom={20}>
            <AppAvailabilitySidebar property={property} />
          </StickyBox>
        </Grid>
      </Grid> */}
    </main>
  );
};

export default withRoot(Dashboard);
