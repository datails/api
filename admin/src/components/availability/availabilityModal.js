import React from "react";
import DatePickerRange from "./DatePickerRange";

const Availability = ({ props }) => {
  return (
    <div>
      <h3>Beschikbaarheid wijzigen</h3>
      <DatePickerRange props={props} />
    </div>
  );
};

export default Availability;
