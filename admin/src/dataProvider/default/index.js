import { stringify } from "query-string";
import httpClient from "../httpClient/index";

export default async (type, resource, params) => {
  if (!params) {
    return;
  }

  const URI =
    process.env.NODE_ENV !== "production"
      ? process.env.REACT_APP_DEV_URI
      : process.env.REACT_APP_PROD_URI;

  switch (type) {
    case "GET_LIST":
    case "GET_MANY_REFERENCES":
      const { page, perPage } = params.pagination;
      const { field, order } = params.sort;
      const query = {
        sort: JSON.stringify([field, order]),
        range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
        filter: JSON.stringify(params.filter),
      };

      return httpClient(`${URI}/${resource}/many?${stringify(query)}`).then(
        ({ json }) => ({
          data:
            json?.data?.map?.((record) => {
              const id = record._id;
              delete record._id;
              return { id, ...record };
            }) || [],
          total: json?.total || 0,
        })
      );

    case "GET_MANY":
      const qry = {
        filter: JSON.stringify({ id: params.ids }),
      };

      return httpClient(`${URI}/${resource}/many?${stringify(qry)}`).then(
        ({ json }) => ({
          data:
            json?.data?.map?.((record) => {
              const id = record._id;
              delete record._id;
              return {
                id,
                ...record,
              };
            }) || [],
          total: json?.total,
        })
      );

    case "GET_ONE":
      return httpClient(`${URI}/${resource}/${params.id}`).then(({ json }) => ({
        data: json.data,
        id: json.data._id,
      }));

    case "UPDATE":
      return (async () => {
        return httpClient(`${URI}/${resource}/${params.id}`, {
          method: "PUT",
          body: JSON.stringify(params.data),
        }).then(({ json }) => ({ data: json.data }));
      })();

    case "UPDATE_MANY":
      return (async () => {
        return httpClient(`${URI}/${resource}/many/${params.id}`, {
          method: "PUT",
          body: JSON.stringify(params.data),
        }).then(({ json }) => ({ data: json.data }));
      })();

    case "DELETE":
      return httpClient(`${URI}/${resource}/${params.id}`, {
        method: "DELETE",
      }).then(({ json }) => {
        return { data: json.data };
      });

    case "DELETE_MANY":
      const param = {
        filter: JSON.stringify(params.ids),
      };

      return httpClient(
        `${URI}/${resource}/many/${params.id}?${stringify(param)}`,
        {
          method: "DELETE",
          body: JSON.stringify(params),
        }
      ).then(({ json }) => ({ data: json }));

    case "CREATE":
      return (async () => {
        return httpClient(`${URI}/${resource}`, {
          method: "POST",
          body: JSON.stringify(params.data),
        }).then(({ json }) => {
          return {
            data: { ...params.data, id: json.data._id },
          };
        });
      })();

    default:
  }
};
