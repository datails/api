import React, { createContext, useReducer } from "react";
import Reducer from "./reducer";

const initialState = JSON.parse(
  window.localStorage.getItem("root:state") ||
    JSON.stringify({
      drawer: false,
      loader: false,
      client: {},
      search: {
        adults: 0,
        babies: 0,
        children: 0,
        guests: 0,
      },
    })
);

const Store = ({ children }) => {
  const [state, dispatch] = useReducer(Reducer, initialState);
  return (
    <Context.Provider value={[state, dispatch]}>{children}</Context.Provider>
  );
};

export const Context = createContext(initialState);

export default Store;
