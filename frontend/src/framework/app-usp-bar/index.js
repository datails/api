import React from "react";
import { Grid, Typography } from "@material-ui/core";
import AppUSPCard from "../../components/app-usp-bar";
import AppButton from "../../components/app-button";
import useStyles from "./app-usp-bar.styles";

export default function ComponentTopBanner({ title, backgroundColor }) {
  const classes = useStyles();

  return (
    <Grid
      container
      className={classes.container}
      justify="space-evenly"
      spacing={10}
      style={{ backgroundColor: backgroundColor }}
    >
      <Grid item lg={12} xl={8} justify="center" align="center">
        <Grid container spacing={10} justify="space-evenly">
          <Grid item sm={12} justify="center" align="center">
            <Typography
              variant="h2"
              component="h2"
              gutterBottom
              className={classes.title}
            >
              {title || "Huren zonder tussenkomst"}
            </Typography>
            <Typography variant="subtitle1" component="h3">
              Huren van woningen via vakantie-vorden biedt de mogelijkheid om
              direct
              <br />
              met de eigenaar in contact te komen zonder tussenkomst van derden.
              <br />
              Vakantie-vorden kenmerkt zich door het aanbieden van unieke
              woningen bij bekende aanbieders.
            </Typography>
          </Grid>
          <Grid item md={12} lg={6} className={classes.content}>
            <AppUSPCard
              background="cabin_house_vorden.jpg"
              desc="Vind de unieke vakantie accomodatie in de Achterhoek."
              title="Zoeken"
            >
              1
            </AppUSPCard>
          </Grid>
          <Grid item md={12} lg={6} className={classes.content}>
            <AppUSPCard
              background="round_house_vorden.jpg"
              desc="Bekijk of de woning beschikbaar is op de geselecteerde data en boek direct!"
              title="Boeken"
            >
              2
            </AppUSPCard>
          </Grid>
          <Grid item md={12} lg={6} className={classes.content}>
            <AppUSPCard
              background="special_house_vorden.jpg"
              desc="Geniet van een heerlijke vakantie in de Achterhoek."
              title="Genieten"
            >
              3
            </AppUSPCard>
          </Grid>
          <Grid item sm={12} justify="center" align="center">
            <AppButton
              styles={{
                "min-width": "250px",
                "font-size": "1.1rem",
                height: "60px",
              }}
              href="/vakantiehuisjes"
            >
              Huisjes zoeken
            </AppButton>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
