import { createStyles, makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) =>
  createStyles({
    anchor: {
      display: "flex",
      flexDirection: "column",
      fontSize: "1rem",
      lineHeight: 2,
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontWeight: 300,
      color: "#FFF",
    },
    container: {
      background: theme.palette.secondary.light,
      margin: "0",
      width: "100%",
      padding: "40px",
      [theme.breakpoints.down("sm")]: {
        padding: "40px 0",
      },
    },
    content: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "flex-start",
      padding: "40px",
    },
    image: {
      margin: "35px 0",
      width: "100%",
    },
    margin: {
      margin: theme.spacing(1),
    },
    title: {
      fontSize: "1rem !important",
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontWeight: 500,
      marginBottom: "1.5rem",
      color: "#FFF",
    },
    text: {
      fontSize: "1rem",
      lineHeight: 1.2,
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontWeight: 300,
      textAlign: "justify",
      color: "#FFF",
    },
    backgroundMain: {
      padding: "0",
      textAlign: "center",
      background: theme.palette.secondary.main,
    },
    colorLight: {
      color: "#FFF",
      fontWeight: 700,
    },
  })
);
