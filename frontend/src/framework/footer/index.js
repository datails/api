import React from "react";
import { Grid, Link, Typography } from "@material-ui/core";
import classNames from "classnames";
import useStyles from "./footer.styles";
import withRoot from "../../theme/withRoot";

function Footer() {
  const classes = useStyles();

  return (
    <footer>
      <Grid container className={classes.container} justify="space-around">
        <Grid item xs={12} sm={12} md={6} lg={4} className={classes.content}>
          <Typography variant="h2" gutterBottom className={classes.title}>
            Vakantiehuisjes Vorden
          </Typography>
          <Typography variant="body1" gutterBottom className={classes.text}>
            Vind het vakantiehuisjes in Vorden dat bij je past! Op zoek naar een
            rustig vakantiehuisjes, een chalet in de bossen of een Bed and
            Breakfast in de Achterhoek? Vind het bij Vakantiehuisje Vorden.
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={4} className={classes.content}>
          <Typography variant="h2" gutterBottom className={classes.title}>
            Contact
          </Typography>
          <Typography variant="body1" className={classes.anchor}>
            Ronald &amp; Ellen <br />
            <Link
              href="mailto:info@vakantiehuisjes-vorden.nl"
              target="_blank"
              color="inherit"
            >
              info@vakantiehuisjes-vorden.nl
            </Link>
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={4} className={classes.content}>
          <Typography variant="h2" gutterBottom className={classes.title}>
            Locatie
          </Typography>
        </Grid>
      </Grid>
      <Grid
        container
        className={classNames([classes.container, classes.backgroundMain])}
        justify="space-around"
      >
        <Grid item xs={12} className={classes.content}>
          <Typography variant="body1" className={classes.anchor}>
            <Link
              href="//datails.nl"
              target="_blank"
              color="inherit"
              className={classes.colorLight}
            >
              Powered By DATAILS.
            </Link>
          </Typography>
        </Grid>
      </Grid>
    </footer>
  );
}

export default withRoot(Footer);
