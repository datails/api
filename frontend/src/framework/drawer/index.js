import React, { useContext } from "react";
import { navigate } from "hookrouter";
import {
  List,
  Divider,
  ListItem,
  ListItemText,
  SwipeableDrawer,
} from "@material-ui/core";
import { Context } from "../../store/store";

import withRoot from "../../theme/withRoot";
import useStyles from "./drawer.styles";

function AppDrawer() {
  const [state, dispatch] = useContext(Context);

  const updateDrawer = () => {
    dispatch({
      type: "TOGGLE_DRAWER",
    });
  };

  const classes = useStyles();

  const sideList = (
    <div className={classes.list}>
      <Divider />
      <List>
        <ListItem
          button
          className={classes.drawerLink}
          onClick={() => navigate("/")}
        >
          <ListItemText primary={"Home"} className={classes.drawerText} />
        </ListItem>
        <ListItem
          button
          className={classes.drawerLink}
          onClick={() => navigate("/vakantiehuisjes")}
        >
          <ListItemText
            primary={"Vakantiehuisjes"}
            className={classes.drawerText}
          />
        </ListItem>
        <ListItem
          button
          className={classes.drawerLink}
          onClick={() => navigate("/bed-and-breakfasts")}
        >
          <ListItemText primary={"B&B's"} className={classes.drawerText} />
        </ListItem>
        <ListItem
          button
          className={classes.drawerLink}
          onClick={() => navigate("/chalets")}
        >
          <ListItemText primary={"Chalets"} className={classes.drawerText} />
        </ListItem>
        <ListItem
          button
          className={classes.drawerLink}
          onClick={() => navigate("/contact")}
        >
          <ListItemText primary={"Contact"} className={classes.drawerText} />
        </ListItem>
        <ListItem
          button
          className={classes.drawerLink}
          onClick={() => navigate("/privacyverklaring")}
        >
          <ListItemText
            primary={"Privacyverklaring"}
            className={classes.drawerText}
          />
        </ListItem>
      </List>
    </div>
  );

  return (
    <div>
      <SwipeableDrawer
        open={state.drawer}
        onClose={updateDrawer}
        onOpen={updateDrawer}
      >
        <div
          tabIndex={0}
          role="button"
          onClick={updateDrawer}
          onKeyDown={updateDrawer}
        >
          {sideList}
        </div>
      </SwipeableDrawer>
    </div>
  );
}

export default withRoot(AppDrawer);
