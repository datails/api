import React from "react";

export const SchemaContext = React.createContext({
  schemas: {},
  setSchemas: () => {},
});

export const PropertyContext = React.createContext({
  properties: {},
  setProperties: () => {},
});

export const ServicesContext = React.createContext({
  services: {},
  setServices: () => {},
});
