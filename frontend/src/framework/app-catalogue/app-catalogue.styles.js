import { makeStyles, createStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    container: {
      margin: "0 auto",
      width: "100%",
      maxWidth: "1200px",
      padding: "40px 0px",
      display: "flex",
      alignContent: "center",
      alignItems: "center",
      justifyContent: "center",
    },
    list: {
      margin: "0",
      width: "100%",
      display: "flex",
      alignContent: "center",
      alignItems: "center",
      [theme.breakpoints.down("sm")]: {
        padding: "40px 0",
      },
    },
    content: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "flex-start",
      alignSelf: "flex-start",
      padding: "20px !important",
    },
    searchBar: {
      border: `1px solid ${theme.palette.primary.light}`,
      display: "flex",
      justifyContent: "center",
      background: "rgb(255 255 255 / 90%)",
      // borderRadius: "100px",
      padding: "15px",
    },
    listCatalogue: {
      padding: "40px 0 !important",
    },
  })
);
