import { makeStyles, createStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    container: {
      margin: "0",
      width: "100%",
    },
    content: {
      display: "flex",
      flexDirection: "row",
      padding: "0px !important",
      width: "100%",
    },
    flexDirectionColumn: {
      flexDirection: "column",
    },
    card: {
      width: "100%",
      // boxShadow: "-7px 7px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
      border: `1px solid ${theme.palette.primary.main}`,
      "&:hover": {
        boxShadow:
          "-8px 11px 13px 0px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
      },
    },
    media: {
      height: 220,
      maxWidth: 220,
      minWidth: 150,
    },
    textDecorationNone: {
      textDecoration: "none!important",
    },
    padding: {
      padding: "25px 15px !important",
    },
    cardActions: {
      display: "flex",
      justifyContent: "space-around",
      width: "100%",
    },
    title: {
      fontWeight: 500,
    },
    smallIcons: {
      fontSize: "16px",
      margin: "5px",
    },
    vertAlign: {
      display: "flex",
      alignItems: "center",
    },
    cta: {
      marginRight: "5px",
    },
    backgroundLight: {
      background: `#ebefdf`,
      borderTop: `1px solid ${theme.palette.primary.main}`,
    },
    button: {
      background: theme.palette.primary.main,
      padding: 5,
      color: "#FFF",
      "&:hover": {
        background: theme.palette.primary.dark,
      },
    },
  })
);
