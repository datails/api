export function isBabies(property, state) {
  return state.search.babies ? property.isFitForBabies : true;
}

export function isMaxGuest(property, state) {
  return property.persons >= state.search.adults + state.search.children;
}

export function isAllServicesOnProperty(property, services) {
  for (const name in services) {
    if (services[name] === true && property[name] !== services[name]) {
      return false;
    }
  }

  return true;
}
