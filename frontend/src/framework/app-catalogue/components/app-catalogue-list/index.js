import React, { useContext } from "react";
import { Grid, Typography } from "@material-ui/core";

import withRoot from "../../../../theme/withRoot";
import { Context } from "../../../../store/store";
import AppCatalogueItem from "../app-catalogue-item";

import useStyles from "./app-catalogue-list.styles";
import * as utils from "./app-catalogue-list.utils";

function AppCatalogueList({ services, properties }) {
  const classes = useStyles();
  const [state] = useContext(Context);

  const filterOnSearch = properties.filter(
    (property) =>
      utils.isBabies(property, state) && utils.isMaxGuest(property, state)
  );
  const filterOnServices = filterOnSearch.filter((property) =>
    utils.isAllServicesOnProperty(property, services)
  );

  return (
    <Grid
      container
      justify="space-around"
      className={classes.list}
      spacing={10}
    >
      {filterOnServices.length === 0 ? (
        <Typography
          gutterBottom
          variant="h5"
          component="h2"
          className={classes.notFoundTitle}
        >
          Geen woningen gevonden!
        </Typography>
      ) : (
        filterOnServices.map((item) => {
          return (
            <Grid item xs={12} className={classes.listCatalogue}>
              <AppCatalogueItem item={item} />
            </Grid>
          );
        })
      )}
    </Grid>
  );
}

export default withRoot(AppCatalogueList);
