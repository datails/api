import { makeStyles, createStyles } from "@material-ui/core";

export default makeStyles(() =>
  createStyles({
    list: {
      margin: "0",
      width: "100%",
      display: "flex",
      alignContent: "center",
      alignItems: "center",
      padding: "20px 0 !important",
    },
    listCatalogue: {
      padding: "20px 0 !important",
    },
    notFoundTitle: {
      margin: "40px 0",
    },
  })
);
