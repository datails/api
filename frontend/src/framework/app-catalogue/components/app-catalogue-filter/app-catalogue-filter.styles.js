import { makeStyles, createStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    sideSearch: {
      border: `1px solid ${theme.palette.primary.light}`,
      display: "flex",
      flexDirection: "column",
      padding: "25px",
      borderRadius: "25px",
    },
    formControl: {
      display: "flex",
      width: "100%",
    },
    sideBarTitle: {
      marginBottom: "30px",
      fontWeight: 500,
    },
  })
);
