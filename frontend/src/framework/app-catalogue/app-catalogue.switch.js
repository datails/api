import React, { useContext, useEffect } from "react";
import { SchemaContext, PropertyContext } from "./app-catalogue.context";
import AppLoader from "../loader";
import withRoot from "../../theme/withRoot";
import * as U from "../../utils"

function CatalogueSwitcher() {
  const { setSchemas } = useContext(SchemaContext);
  const { setProperties } = useContext(PropertyContext);

  const loadSchemas = async () => {
    const schemas = await U.loadSchemas();
    setSchemas(schemas);
  };

  const loadProperties = async () => {
    const properties = await U.loadProperties();

    setProperties(properties);
  };

  useEffect(() => {
    loadSchemas();
    loadProperties();
  }, []);

  return <AppLoader visible={true} />;
}

export default withRoot(CatalogueSwitcher);
