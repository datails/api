import { makeStyles } from "@material-ui/core";

export default makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 999999,
    color: "#fff",
    flexDirection: "column",
  },
}));
