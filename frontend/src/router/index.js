import React from "react";
import { useRoutes, useInterceptor } from "hookrouter";
import AppHome from "../pages/home";
import AppProperties from "../pages/properties";
import AppProperty from "../pages/property";
import AppReservations from "../pages/reservations";
import AppBooking from "../pages/booking";
import Page404 from "../pages/404";

const routes = {
  "/": () => <AppHome />,
  "/vakantiehuisjes": () => <AppProperties />,
  "/vakantiehuisjes/:id": ({ id }) => <AppProperty id={id} />,
  "/vakantiehuisjes/:id/reserveren": ({ id }) => <AppReservations id={id} />,
  "/vakantiehuisjes/:id/boeking/:bookingId": ({ id, bookingId }) => <AppBooking id={id} bookingId={bookingId} />,
};

const App = () => {
  useInterceptor((currentPath, nextPath) => {
    window.scrollTo(0, 0);
    return nextPath;
  });

  const routeResult = useRoutes(routes);
  return routeResult || <Page404 />;
};

export default App;
