import React from "react";
import { Divider } from "@material-ui/core";
import { Helmet } from "react-helmet";
import AppTextArea from "./components/text-area";
import AppTextAreaBottom from "./components/text-area-bottom";
import AppHeader from "./components/header";
import TopBanner from "../../framework/app-usp-bar";
import withRoot from "../../theme/withRoot";

function AppHome() {
  return (
    <React.Fragment>
      <Helmet>
        <title>
          Compleet boekingssyteem op je eigen website.
        </title>
        <meta
          name="description"
          content="Vind het vakantiehuisjes in Vorden dat bij je past! Op zoek naar een rustig vakantiehuisjes, een chalet in de bossen of een Bed and Breakfast in de Achterhoek? Vind het bij Vakantiehuisje Vorden."
        />
        <meta
          property="og:description"
          content="Vind het vakantiehuisjes in Vorden dat bij je past! Op zoek naar een rustig vakantiehuisjes, een chalet in de bossen of een Bed and Breakfast in de Achterhoek? Vind het bij Vakantiehuisje Vorden."
        />
        <meta
          property="og:title"
          content="Vakantiehuisjes Vorden | Vind de vakantie woning die past bij dit moment!"
        />
        <meta property="og:url" content={"https://vakantiehuisjes-vorden.nl"} />
      </Helmet>
      <AppHeader />
      <Divider />
      {/* <TopCallToAction /> */}
      {/* <Divider /> */}
      <TopBanner />
      <Divider />
      <AppTextArea />
      <Divider />
      <AppTextAreaBottom />
      <Divider />
    </React.Fragment>
  );
}

export default withRoot(AppHome);
