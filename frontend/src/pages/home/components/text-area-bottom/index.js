import React from "react";
import { AppButton, AppTextImage } from "../../../../components";
import withRoot from "../../../../theme/withRoot";

function TextArea() {
  return (
    <AppTextImage
      title="Start eigen portal"
      reverse
      text={`Start een eigen woningportaal inclusief bookingsysteem. Ideaal voor starters die alles in eigen beheer willen houden.`}
      image={require("../../../../assets/cottage.jpg")}
    >
      <AppButton styles={{ maxWidth: "200px", margin: 0 }} href="/over-ons">
        Word eigenaar
      </AppButton>
    </AppTextImage>
  );
}

export default withRoot(TextArea);
