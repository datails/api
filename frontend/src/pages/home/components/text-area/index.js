import React from "react";
import { AppButton, AppTextImage } from "../../../../components";
import withRoot from "../../../../theme/withRoot";

function TextArea() {
  return (
    <AppTextImage
      title="Word een host"
      reverse={false}
      text={`Plaats eenvoudig je vakantie huisje online en begin vandaag met het genereren van inkomen. Ideaal als je een kleinschalige aanpak wilt, waarbij je weet wie er achter de schermen werken en niet aan onverwachte kosten of voorwaarden moet voldoen.`}
      image={require("../../../../assets/happy_people.jpg")}
    >
      <AppButton styles={{ maxWidth: "200px", margin: 0 }} href="/admin">
        Word host
      </AppButton>
    </AppTextImage>
  );
}

export default withRoot(TextArea);
