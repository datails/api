import { createStyles, makeStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    colorWhite: {
      color: "#fff",
    },
    container: {
      "&::before": {
        content: '""',
        display: "block",
        position: "absolute",
        right: "-50%",
        bottom: "-50%",
        width: "200%",
        height: "100%",
        zIndex: 1,
        opacity: 0.2,
        // background: "#FFF",
        transform: "rotate(30deg)",
      },
      minHeight: "70vh",
      margin: "-65px 0 0",
      backgroundImage: `url(${require(`../../../../assets/holiday_vorden.jpg`)})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      backgroundAttachment: "fixed",
      backgroundPosition: "center",
      width: "100%",
      position: "relative",
      overflow: "hidden",
      [theme.breakpoints.down("sm")]: {
        minHeight: "50vh",
        backgroundAttachment: "inherit",
      },
    },
    content: {
      position: "relative",
      zIndex: 2,
      alignItems: "center",
      display: "flex",
      textAlign: "left",
      flexDirection: "column",
      marginTop: 60,
      // justifyContent: "center",
      [theme.breakpoints.down("sm")]: {
        marginLeft: "0px",
        maxWidth: "100%",
        textAlign: "center",
      },
    },
    title: {
      color: "#095e4b",
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontWeight: 300,
      // textTransform: "uppercase",
      fontSize: "2rem",
      fontStyle: "italic",
      marginTop: 60,
      [theme.breakpoints.up("lg")]: {
        fontSize: "3rem",
      },
    },
    subTitle: {
      marginBottom: "25px",
      color: "#095e4b",
      fontWeight: 400,
      fontSize: "22px",

      [theme.breakpoints.down("sm")]: {
        display: "none",
      },
    },
    searchBar: {
      background: "rgb(255 255 255 / 90%)",
      borderRadius: "100px",
      padding: "5px 25px",
    },
  })
);
