import React from "react";
import { Grid, Typography } from "@material-ui/core";
import useStyles from "./header.styles";

export default function Header() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid container spacing={10} className={classes.container}>
        <Grid item xs={12} sm={12} md={12} className={classes.content}>
          <Typography
            variant="h1"
            component="h1"
            gutterBottom
            className={classes.title}
          >
            Gebruik het Pluggable bookingssysteem in je eigen website
          </Typography>
          <Typography
            variant="h2"
            component="h2"
            gutterBottom
            className={classes.subTitle}
          >
            Native integratie
          </Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
