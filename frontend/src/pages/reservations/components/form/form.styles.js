import { makeStyles } from "@material-ui/core";

export default makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
    },
  },
  textfield: {
    width: "100%",
    marginBottom: 35,
  },
  paper: {
    width: "100%",
    maxWidth: "450px",
    minHeight: "500px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontFamily:
      "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif !important",
    padding: "0 40px",
    margin: 0,
  },
  form: {
    display: "flex",
    width: "100%",
    alignItems: "flex-start",
    justifyContent: "space-between",
    fontFamily:
      "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif !important",
  },
  formHidden: {
    display: "none",
  },
  container: {
    margin: "0",
    width: "100%",
    padding: "40px",
  },
  content: {
    display: "flex",
    justifyContent: "center",
    fontFamily:
      "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
  },
  button: {
    color: "#FFF",
    fontSize: "1.1rem",
    height: "60px",
    minWidth: "175px",
    backgroundColor: theme.palette.primary.main,
    "&:hover": {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  subTitle: {
    fontWeight: 500,
  },
}));
