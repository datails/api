import React, { useContext } from "react";
import { Typography } from "@material-ui/core";
import { Context } from "../../../../store/store";
import useStyles from "./text-area.styles";

export default function AppText() {
  const classes = useStyles();
  const [state] = useContext(Context);
  const isValidBooking =
    state.property && state.search.checkin && state.search.checkout;

  if (!isValidBooking) return null;

  return (
    <div className={classes.margin}>
      <Typography gutterBottom variant="h1" component="h1">
        Reserveren van {state.property.title}
      </Typography>
      <Typography gutterBottom variant="h5" component="h2">
        U bent bijna klaar om een reservering te plaatsen van{" "}
        {state.property.title}. Van{" "}
        {new Date(state.search.checkin).toLocaleDateString()} tot{" "}
        {new Date(state.search.checkout).toLocaleDateString()}. Let op dat er
        aan de reservering geen rechten kunnen worden ontleend. Goedkeuring en
        betaling gaat via contact met de eigenaar.
      </Typography>
    </div>
  );
}
