import React, { useState } from "react";
import { Helmet } from "react-helmet";
import { Divider, Grid } from "@material-ui/core";
import StickyBox from "react-sticky-box";

import withRoot from "../../theme/withRoot";
import { AppForm, AppHeader } from "./components";
import { AppBreadCrumb } from "../../framework";
import useStyles from "./reservations.styles";

import ReservationSidebar from "./components/property-sidebar";
import PropertyContext from "../property/property.context";
import PropertySwitcher from "../property/property.switch";

function AppReservations({ id }) {
  const classes = useStyles();
  const [property, setProperty] = useState();
  const value = { property, setProperty };

  if (!property) {
    return (
      <PropertyContext.Provider value={value}>
        <PropertySwitcher id={id} />
      </PropertyContext.Provider>
    );
  }

  return (
    <React.Fragment>
      <Helmet>
        <title>Reserveer een vakantiewoning in Vorden</title>
        <meta
          name="description"
          content="Reserveer het vakantiehuis in Vorden dat bij u past! Reserveer bed-and-breakfast of complete vakantiewoningen."
        />
        <meta
          property="og:description"
          content="Reserveer het vakantiehuis in Vorden dat bij u past! Reserveer bed-and-breakfast of complete vakantiewoningen."
        />
        <meta
          property="og:title"
          content="Reserveer een vakantiewoning in Vorden"
        />
        <meta
          property="og:url"
          content={"https://vakantiehuisjes-vorden.nl/reserveren"}
        />
      </Helmet>
      <Divider />
      <AppHeader />
      <AppBreadCrumb backgroundColor="#525748" current={property.title} />
      <Divider />
      <Grid
        container
        className={classes.container}
        justify="space-evenly"
        spacing={10}
      >
        <Grid item xs={12} md={8} justify="center" align="center">
          <AppForm />
        </Grid>
        <Grid item xs={12} md={4} justify="center" align="center">
          <StickyBox offsetTop={100} offsetBottom={20}>
            <ReservationSidebar property={property} />
          </StickyBox>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default withRoot(AppReservations);
