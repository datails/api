import React from "react";
import AppHeader from "../../../../components/app-hero-header";

export default function () {
  return (
    <AppHeader
      subTitle="Niet gevonden"
      title="404"
      background="holiday_vorden.jpg"
    />
  );
}
