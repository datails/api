import React from "react";

const PropertyContext = React.createContext({
  property: {},
  setProperty: () => {},
});

export default PropertyContext;
