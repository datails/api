import React from "react";
import { Avatar, Grid, Typography, Divider } from "@material-ui/core";
import { Helmet } from "react-helmet";
import AppSlider from "../../../../components/app-carousel";
import { AppDateRangePicker, AppDetails, AppProvisions } from "./components";

import withRoot from "../../../../theme/withRoot";
import usePropertyStles from "./property.styles";

const Property = ({ property }) => {
  const classes = usePropertyStles();

  return (
    <article>
      <Helmet>
        <title>{property.title}</title>
        <meta name="description" content={property.subtitle} />
        <meta property="og:image" content={property.url} />
        <meta property="og:description" content={`${property.title}`} />
        <meta property="og:title" content={property.title} />
        <meta
          property="og:url"
          content={`https://vakantiehuisjes-vorden.nl/vakantiehuisjes/${property.id}`}
        />
        <meta property="og:type" content="article" />
        <meta property="og:site_name" content="vakantiehuisjes-vorden.nl" />
      </Helmet>
      <Grid container className={classes.container} justify="space-evenly">
        <Grid item xs={12} justify="center" align="center">
          <AppSlider items={property.files} />
          <Typography
            component="h1"
            variant="h4"
            gutterBottom
            className={classes.title}
          >
            {property.title}
            {/* <Avatar
              alt={property.owner.name}
              src={property.owner.file.filename}
              className={classes.avatar}
            /> */}
          </Typography>
          <Typography
            component="h2"
            variant="body1"
            gutterBottom
            className={classes.subTitle}
          >
            {property.persons} gasten | {property.beds} bedden | minimaal{" "}
            {property.minNights} nachten | {property.squareMeters} vierkante
            meter
          </Typography>
          <Divider className={classes.divider} />
          <AppDetails property={property} />
          <Divider className={classes.divider} />
          <section
            className={classes.desc}
            dangerouslySetInnerHTML={{ __html: property.description }}
          ></section>
          <Divider className={classes.divider} />
          <AppProvisions property={property} />
          <Divider className={classes.divider} />
          <AppDateRangePicker property={property} />
        </Grid>
      </Grid>
    </article>
  );
};

export default withRoot(Property);
