import { makeStyles } from "@material-ui/core";

export default makeStyles(() => ({
  root: {
    marginTop: 20,
    display: "flex",
    justifyContent: "flex-start",
    flexWrap: "wrap",
    "& > *": {
      margin: "4px 8px 4px 0",
    },
  },
  title: {
    marginTop: 30,
    textAlign: "left",
  },
}));
