import "moment/locale/nl";
import { parseISO, isWithinInterval } from "date-fns";
import Moment from "moment";
import { extendMoment } from "moment-range";

const moment = extendMoment(Moment);

export function isDayBlocked({ day, blockedDays, minNights }) {
  return blockedDays.some((d) => d.isSame(day, "day"));
}

export function isDateBlocked(start, end, data) {
  const dateFormat = "YYYY-MM-DD";
  const diff = moment(end).diff(start, "days") + 1;

  for (let i = 0; i < diff; i++) {
    const checkDate = moment(start).add(i, "d").format(dateFormat);

    const item = data.find((d) => {
      return moment(d).format("YYYY-MM-DD") === checkDate;
    });

    if (item) {
      return true;
    }
  }

  return false;
}

export function isDayBetweenDate(day, availability) {
  return availability?.some?.(({ startDate, endDate }) =>
    isWithinInterval(day, {
      start: parseISO(startDate),
      end: parseISO(endDate),
    })
  );
}

export function getTimeSlot(day, availability) {
  return availability.find(({ startDate, endDate }) =>
    isWithinInterval(day, {
      start: parseISO(startDate),
      end: parseISO(endDate),
    })
  );
}

export function getCorrectPrice(price, day, availability) {
  const isDateBetweenDates = isDayBetweenDate(day, availability);

  if (!isDateBetweenDates) {
    return price;
  }

  const timeSlot = getTimeSlot(day, availability);

  return timeSlot.price;
}
