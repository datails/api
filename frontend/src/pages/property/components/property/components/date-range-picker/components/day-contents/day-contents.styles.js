import { makeStyles } from "@material-ui/core";

export default makeStyles(() => ({
  dayContentWrapper: {
    position: "relative",
  },
  dayContentPrice: {
    position: "absolute",
    top: 0,
    left: 10,
    marginTop: "-30%",
    fontSize: "10px",
  },
}));
