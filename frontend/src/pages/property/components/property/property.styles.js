import { makeStyles, createStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    container: {
      margin: "0 auto",
      width: "100%",
      maxWidth: 1200,
    },
    content: {
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      fontSize: "1.1rem",
      textAlign: "left",
      fontWeight: 300,
      lineHeight: 1.8,
      padding: "40px",
    },
    title: {
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      margin: 0,
      textAlign: "left",
      textTransform: "none",
      fontSize: 28,
      display: "flex",
      justifyContent: "space-between",
      alignContent: "center",
      alignItems: "center",
    },
    subTitle: {
      textAlign: "left",
      marginBottom: 40,
    },
    details: {
      textAlign: "left",
      textTransform: "none",
      fontSize: 24,
    },
    desc: {
      textAlign: "left",
      textTransform: "none",
      fontSize: 18,
    },
    card: {
      borderTopRightRadius: "45px",
    },
    avatar: {
      width: theme.spacing(7),
      height: theme.spacing(7),
      display: "inline-flex",
    },
    divider: {
      margin: "40px 0",
    },
  })
);
