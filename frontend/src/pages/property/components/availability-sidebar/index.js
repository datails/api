import React from "react";
import { Paper, Typography } from "@material-ui/core";
import withRoot from "../../../../theme/withRoot";
import AppBooking from "../../../../components/app-booking";

import useAvailabilitySideBarStyles from "./availability-sidebar.styles";

function AppAvailabilitySidebar({ property }) {
  const classes = useAvailabilitySideBarStyles();

  return (
    <Paper
      component="aside"
      className={classes.sideSearch}
      id="search-container"
    >
      <Typography
        gutterBottom
        variant="h5"
        component="h2"
        className={classes.sideBarTitle}
      >
        Direct boeken
      </Typography>
      <AppBooking initialProperty={property} />
    </Paper>
  );
}

export default withRoot(AppAvailabilitySidebar);
