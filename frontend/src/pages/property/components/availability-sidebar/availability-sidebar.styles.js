import { makeStyles, createStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    sideSearch: {
      border: `1px solid ${theme.palette.primary.main}`,
      display: "flex",
      flexDirection: "column",
      padding: "25px",
      borderRadius: "25px",
    },
  })
);
