import React, { useState } from "react";
import { Grid, Divider } from "@material-ui/core";
import StickyBox from "react-sticky-box";
import { AppBreadCrumb } from "../../framework";
import {
  AppProperty,
  AppPropertyHeader,
  AppAvailabilitySidebar,
} from "./components";
import withRoot from "../../theme/withRoot";

import PropertyContext from "./property.context";
import PropertySwitcher from "./property.switch";
import usePropertyStyles from "./property.styles";

const VakantieHuis = ({ id }) => {
  const classes = usePropertyStyles();
  const [property, setProperty] = useState();
  const value = { property, setProperty };

  if (!property)
    return (
      <PropertyContext.Provider value={value}>
        <PropertySwitcher id={id} />
      </PropertyContext.Provider>
    );

  return (
    <main>
      <AppPropertyHeader />
      <AppBreadCrumb backgroundColor="#4092B8" current={property.title} />
      <Divider />
      <Grid
        container
        className={classes.container}
        justify="space-evenly"
        spacing={10}
      >
        <Grid item xs={12} md={8} justify="center" align="center">
          <AppProperty property={property} />
        </Grid>
        <Grid item xs={12} md={4} justify="center" align="center">
          <StickyBox offsetTop={100} offsetBottom={20}>
            <AppAvailabilitySidebar property={property} />
          </StickyBox>
        </Grid>
      </Grid>
    </main>
  );
};

export default withRoot(VakantieHuis);
