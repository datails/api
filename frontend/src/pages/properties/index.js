import React from "react";
import { Helmet } from "react-helmet";
import { Divider } from "@material-ui/core";
import withRoot from "../../theme/withRoot";
import { AppHeader } from "./components";
// import AppText from "./components/text";
import { AppCatalogue, AppBreadCrumb } from "../../framework";

function AppVakantieHuisjes() {
  return (
    <React.Fragment>
      <Helmet>
        <title>Vakantiewoningen in Vorden</title>
        <meta
          name="description"
          content="Vind het vakantiehuis in Vorden dat bij u past! Kies uit bed-and-breakfast of complete vakantiewoningen."
        />
        <meta
          property="og:description"
          content="Vind het vakantiehuis in Vorden dat bij u past! Kies uit bed-and-breakfast of complete vakantiewoningen."
        />
        <meta property="og:title" content="Vakantiewoningen in Vorden" />
        <meta
          property="og:url"
          content={"https://vakantiehuisjes-vorden.nl/vakantiehuisjes"}
        />
      </Helmet>
      <Divider />
      <AppHeader />
      <AppBreadCrumb backgroundColor="#4092B8" />
      <Divider />
      <AppCatalogue />
      {/* <AppText /> */}
      <Divider />
    </React.Fragment>
  );
}

export default withRoot(AppVakantieHuisjes);
