import React from "react";
import { AppHeroHeader } from "../../../../components";
import withRoot from "../../../../theme/withRoot";

function AppHeader() {
  return (
    <AppHeroHeader
      subTitle="Een vakantie die bij je past!"
      title="Vakantiehuisjes in vorden"
      background="background_catalogue.jpg"
    />
  );
}

export default withRoot(AppHeader);
