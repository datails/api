import React, { useState } from "react";
import { Helmet } from "react-helmet";
import { Divider, Grid } from "@material-ui/core";

import withRoot from "../../theme/withRoot";
import { AppHeader } from "./components";
import { AppBreadCrumb } from "../../framework";
import useStyles from "./booking.styles";

import PropertyContext from "../property/property.context";
import PropertySwitcher from "../property/property.switch";

import { BookingContext } from "./booking.context";
import BookingSwitch from "./booking.switch"

function AppBooking({ id, bookingId }) {
  const classes = useStyles();
  const [property, setProperty] = useState();
  const [reservation, setReservation] = useState({});

  if (!property || !reservation) {
    return (
      <PropertyContext.Provider value={{ property, setProperty }}>
        <BookingContext.Provider value={{ reservation, setReservation }}>
          <PropertySwitcher id={id} />
          <BookingSwitch id={bookingId} />
        </BookingContext.Provider>
      </PropertyContext.Provider>
    );
  }

  return (
    <React.Fragment>
      <Helmet>
        <title>Uw booking van vakantiewoning {property.title}</title>
        <meta
          name="description"
          content="Bekijk de reservering bij uw vakantiewoning."
        />
        <meta
          property="og:description"
          content="Bekijk de reservering bij uw vakantiewoning."
        />
        <meta
          property="og:title"
          content="Bekijk de reservering bij uw vakantiewoning."
        />
        <meta
          property="og:url"
          content={"https://vakantiehuisjes-vorden.nl"}
        />
      </Helmet>
      <Divider />
      <AppHeader />
      <AppBreadCrumb backgroundColor="#525748" />
      <Divider />
      <Grid
        container
        className={classes.container}
        justify="space-evenly"
        spacing={10}
      >
        <Grid item xs={12} md={8} justify="center" align="center">

        </Grid>
        <Grid item xs={12} md={4} justify="center" align="center">

        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default withRoot(AppBooking);
