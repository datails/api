import { makeStyles, createStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    backgroundGreen: {
      background: "transparent",
      height: 64,
      width: "100%",
      display: "flex",
      marginTop: "-64px",
    },
  })
);
