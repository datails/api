import { makeStyles } from "@material-ui/core";

export default makeStyles({
    container: {
      margin: "0",
      width: "100%",
    },
    content: {
      display: "flex",
      flexDirection: "row",
      padding: "0px !important",
      width: "100%",
    },
    card: {
      width: "100%",
      boxShadow: "0 0 2.5rem 0.3125rem rgba(0,0,0,.1)",
    },
    media: {
      height: 200,
      maxWidth: 200,
    },
    textDecorationNone: {
      textDecoration: "none!important",
    },
    padding: {
      padding: "25px 15px !important",
    },
    cardActions: {
      display: "flex",
      justify: "space-around",
    },
    title: {
      fontWeight: 500,
    },
  });