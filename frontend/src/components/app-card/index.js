import React from "react";
import {
  Card,
  Grid,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import classnames from "classnames";

import useStyles from './app-card.styles';

export default function AppCard({ buttonText, desc, image, title, href }) {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <Grid
        container
        justify="space-around"
        className={classes.container}
        spacing={10}
      >
        <Grid item className={classes.content}>
          <CardMedia
            component="img"
            alt={title}
            height="140"
            image={image}
            title={title}
            className={classes.media}
          />
          <Grid
            container
            justify="space-around"
            className={classes.container}
            spacing={10}
          >
            <Grid item xs={12} className={classes.content}>
              <CardContent
                className={classnames(classes.content, classes.padding)}
              >
                <Typography
                  gutterBottom
                  variant="h5"
                  component="h2"
                  className={classes.title}
                >
                  {title}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  {desc}
                </Typography>
              </CardContent>
            </Grid>
            <Grid item xs={12} className={classes.content}>
              <CardActions className={classes.cardActions}>
                <Typography
                  gutterBottom
                  variant="h5"
                  component="h2"
                ></Typography>
                <Button href={href} size="small" color="primary">
                  {buttonText}
                </Button>
              </CardActions>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Card>
  );
}
