import { makeStyles } from "@material-ui/core";

export default makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
  fullWidth: {
    width: "100%",
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  dialog: {
    width: "350px",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));
