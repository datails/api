import "date-fns";
import React, { useContext, useEffect, useState } from "react";
import { Grid, Typography } from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import dutchLocale from "date-fns/locale/nl";
import { MuiPickersUtilsProvider, DatePicker } from "@material-ui/pickers";
import classNames from "classnames";
import AppPersons from "../app-search/components/persons";
import { Context } from "../../store/store";

import { validateDatesAndUpdateToValidDateRange } from "../../utils";
import { AppPriceList, AppBookButton } from "./components";
import { AppCirculairLoader } from "..";
import withRoot from "../../theme/withRoot";
import useStyles from "./app-booking.styles";

function AppBooking({ direction = "row", initialProperty = {}, readOnly = false }) {
  const date = new Date();
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  const [state, dispatch] = useContext(Context);

  const handleInvalidDates = () => {
    const { checkin, checkout } = validateDatesAndUpdateToValidDateRange({
      checkin: state.search.checkin,
      checkout: state.search.checkout,
      property: initialProperty,
    });

    dispatch({
      type: "SET_SEARCH",
      search: {
        checkin,
        checkout,
      },
    });

    setLoading(false);
  };

  const handleCheckInChange = (date) => {
    dispatch({
      type: "SET_SEARCH",
      search: {
        checkin: date,
      },
    });
  };

  const handleCheckoutChange = (date) => {
    dispatch({
      type: "SET_SEARCH",
      search: {
        checkout: date,
      },
    });
  };

  useEffect(() => {
    handleInvalidDates();
  }, []);

  if (loading) {
    return <AppCirculairLoader />;
  }

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={dutchLocale}>
      <Grid
        container
        justify="space-between"
        direction={direction}
        className={classes.container}
        spacing={10}
        component={"nav"}
      >
        <Grid className={classes.content}>
          <div>
            <DatePicker
              minDate={date}
              margin="normal"
              id="startDate"
              label="Inchecken"
              format="MM/dd/yyyy"
              value={state.search.checkin}
              onChange={handleCheckInChange}
              title={"Inchecken"}
              cancelLabel={"Annuleren"}
              className={classes.datePicker}
              disabled={readOnly}
            />
          </div>
          {state.search.checkin && (
            <DatePicker
              minDate={state.search.checkin}
              margin="normal"
              id="endDate"
              label="Uitchecken"
              format="MM/dd/yyyy"
              value={state.search.checkout}
              onChange={handleCheckoutChange}
              title={"Uitchecken"}
              cancelLabel={"Annuleren"}
              className={classes.datePicker}
              disabled={readOnly}
            />
          )}
        </Grid>
        <Grid className={classNames(classes.content, classes.justifyCenter)}>
          <AppPersons className={classes.datePicker} />
        </Grid>
        <>
          {!readOnly && (
            <Grid className={classNames(classes.content, classes.justifyCenter)}>
              <AppBookButton property={initialProperty} />
            </Grid>
          )}
          <Grid className={classes.content}>
            <Typography
              component="p"
              variant="body1"
              gutterBottom
              className={classes.subTitle}
            >
              <AppPriceList
                property={initialProperty}
                checkin={state.search.checkin}
                checkout={state.search.checkout}
              />
            </Typography>
          </Grid>
        </>
      </Grid>
    </MuiPickersUtilsProvider>
  );
}

export default withRoot(AppBooking);
