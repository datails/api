import { createStyles, makeStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    button: {
      backgroundColor: theme.palette.primary.main,
    },
  })
);
