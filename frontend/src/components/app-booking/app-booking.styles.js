import { makeStyles } from "@material-ui/core";

export default makeStyles((theme) => ({
  container: {
    margin: "0",
    display: "flex",
    alignContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: "100%",
  },
  content: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 30,
    width: "100%",
  },
  innerPadding: {
    display: "flex",
  },
  innerForm: {
    display: "flex",
    flexBasis: "100%",
  },
  datePicker: {
    margin: "5px",
  },
  borderRight: {
    alignSelf: "center",
    borderRight: "1px solid rgb(81 81 83 / 30%)",
    flex: "0 0 0px",
    height: "32px",
    marginLeft: "20px",
  },
  subTitle: {
    width: "100%",
  },
  marginTop: {
    marginTop: 20,
  },
  justifyCenter: {
    justifyContent: "center",
  },
}));
