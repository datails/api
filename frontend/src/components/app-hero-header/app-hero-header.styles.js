import { createStyles, makeStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    colorWhite: {
      color: "#fff",
    },
    container: {
      minHeight: "450px",
      margin: "-65px 0 0",
      width: "100%",
    },
    content: {
      alignItems: "center",
      // backgroundColor: "rgba(0,0,0, 0.4)",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      padding: "40px",
    },
    title: {
      color: theme.palette.primary.dark,
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      textTransform: "uppercase",
      fontStyle: "italic",
      fontWeight: 300,
      [theme.breakpoints.down("xs")]: {
        fontSize: "2.2rem",
      },
    },
    subTitle: {
      color: theme.palette.primary.main,
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontSize: "1rem",
      [theme.breakpoints.down("sm")]: {
        display: "none",
      },
    },
  })
);
