import React from "react";
import { Grid, Typography } from "@material-ui/core";
import useStyles from "./app-hero-header.styles";

export default function AppHeroHeader({
  subTitle,
  title,
  background,
  local = true,
}) {
  const classes = useStyles();

  const injectBackGroundImage = (image) => ({
    backgroundImage: `url(${image})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundAttachment: "fixed",
  });

  return (
    <React.Fragment>
      <Grid
        container
        spacing={10}
        className={classes.container}
        justify="space-around"
        style={injectBackGroundImage(
          local
            ? require(`../../assets/${background.toLowerCase()}`)
            : background
        )}
      >
        <Grid item xs={12} className={classes.content}>
          <Typography
            component="h1"
            variant="h2"
            align="center"
            color="textPrimary"
            className={classes.title}
            gutterBottom
          >
            {title}
          </Typography>
          {subTitle && (
            <Typography
              variant="h5"
              align="center"
              color="textSecondary"
              className={classes.subTitle}
              paragraph
            >
              {subTitle}
            </Typography>
          )}
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
