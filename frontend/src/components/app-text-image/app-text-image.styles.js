import { createStyles, makeStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    container: {
      margin: "0",
      width: "100%",
      padding: "40px",
      display: "flex",
      alignContent: "center",
      alignItems: "center",
      background: "#FFF",
      [theme.breakpoints.down("sm")]: {
        padding: "40px 0",
      },
    },
    content: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "flex-start",
      padding: "40px",
    },
    image: {
      width: "100%",
      borderRadius: "0.25rem",
      boxShadow: "0 0 2.5rem 0.3125rem rgba(0,0,0,.3)",
    },
    text: {
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontWeight: 300,
      lineHeight: 1.8,
      textAlign: "justify",
      fontSize: "1.1rem",
    },
    title: {
      display: "flex",
      textTransform: "uppercase",
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontWeight: 700,
      fontSize: "2rem",
      position: "relative",
      marginBottom: "2rem",
      lineHeight: 2,
      "&::after": {
        position: "absolute",
        content: '""',
        bottom: 0,
        width: "80px",
        height: "3px",
        background: theme.palette.secondary.dark,
        transform: "translateX(0%)",
        [theme.breakpoints.down("sm")]: {
          display: "none",
        },
      },
    },
  })
);
