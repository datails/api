import * as R from 'ramda';
import * as D from 'date-fns';
import Moment from "moment";
import { extendMoment } from "moment-range";
import "moment/locale/nl";

const moment = extendMoment(Moment);

export const parseDateOrString = R.pipe(
  R.defaultTo(D.addDays(new Date(), 1)),
  R.unless(
    R.is(Date),
    D.parseISO,
  )
);

export const toParseISO = R.unless(
  D.isValid,
  D.parseISO
);

export const isBefore = R.flip(R.curry(D.isBefore))

export const parseCheckinCheckoutDates = R.curryN(3, function(
  checkin,
  checkout,
  minNights
){
  return R.applySpec({
    checkin: R.pipe(
      R.always(checkin),
      toParseISO
    ),
    checkout: R.pipe(
      R.always(checkout),
      toParseISO,
      R.when(
        isBefore(checkin),
        R.always(D.addDays(checkin, minNights))
      )
    )
  })({})
});

export const getDifferenceInDays = (checkin, checkout, minNights) => {
  const dates = parseCheckinCheckoutDates(checkin, checkout, minNights);
  return D.differenceInDays(dates.checkout, dates.checkin);
};

export const defaultToNil = R.defaultTo(0)

export const reduceAvailability = R.curryN(4, function(
  checkin,
  checkout,
  acc,
  availability
){
  const start = parseDateOrString(availability.startDate);
  const end = parseDateOrString(availability.endDate);

  const isCheckinBeforeStartDate = checkin <= start;

  const overlap = D.getOverlappingDaysInIntervals(
    {
      start: isCheckinBeforeStartDate ? parseDateOrString(checkin) : start,
      end: isCheckinBeforeStartDate ? parseDateOrString(checkout) : end,
    },
    {
      start: isCheckinBeforeStartDate ? start : parseDateOrString(checkin),
      end: isCheckinBeforeStartDate ? end : parseDateOrString(checkout),
    }
  );

  return {
    overlap: defaultToNil(acc.overlap) + overlap,
    price: defaultToNil(acc.price) + overlap * availability.price,
  };
});

export const getTotalPriceOfProperty = (
  price,
  checkin,
  checkout,
  availabilities,
  minNights
) => {
  const totalDays = getDifferenceInDays(checkin, checkout, minNights);
  const computedPrice = availabilities.reduce(reduceAvailability(checkin, checkout), {});

  return (
    (totalDays - defaultToNil(computedPrice.overlap)) * defaultToNil(price) +
    defaultToNil(computedPrice.price)
  );
}

export const isDateOverlapWithBooking = (blockedDays, checkin, checkout) => {
  return blockedDays.some((blockedDay) =>
    D.isWithinInterval(blockedDay, {
      start: checkin,
      end: checkout,
    })
  );
}

export const isAnyIntervalOverlapping = (reservations, checkin, checkout) => {
  return reservations.some((reservation) => {
    const start = parseDateOrString(reservation.startDate);
    const end = parseDateOrString(reservation.endDate);
    const isCheckinBeforeStartDate = D.isBefore(checkin, start);

    return D.areIntervalsOverlapping(
      {
        start: isCheckinBeforeStartDate ? checkin : start,
        end: isCheckinBeforeStartDate ? checkout : end,
      },
      {
        start: isCheckinBeforeStartDate ? start : checkin,
        end: isCheckinBeforeStartDate ? end : checkout,
      }
    );
  });
}

export function getNearestAvailabilityDates(
  reservations,
  checkin,
  checkout,
  minNights
) {
  const parsedCheckinCheckout = parseCheckinCheckoutDates(
    checkin,
    checkout,
    minNights
  );

  let i = 0;
  let start = parsedCheckinCheckout.checkin;
  let end = parsedCheckinCheckout.checkout;

  while (isAnyIntervalOverlapping(reservations, start, end)) {
    start = D.addDays(parsedCheckinCheckout.checkin, i);
    end = D.addDays(parsedCheckinCheckout.checkout, i + minNights);

    i++;
  }

  return {
    checkin: start,
    checkout: end,
  };
}

export function isMinimumNights(checkin, checkout, minNights) {
  return D.differenceInDays(checkin, checkout) >= minNights;
}

export function validateDatesAndUpdateToValidDateRange({
  checkin,
  checkout,
  property,
}) {
  const date = new Date();

  // update the dates, as we have moment and date-fns throughtout the project
  // the calendar uses moment, but our api uses date-fns
  checkin = parseDateOrString(checkin) || date;
  checkout = parseDateOrString(checkout) || date;

  // if checkout is before checkin, update both
  if (D.isBefore(checkout, checkin)) {
    checkin = D.addDays(date, 1).toISOString();
    checkout = D.addDays(D.parseISO(checkin), property.minNights).toISOString();
  }

  // if checkin is after, update checkout
  if (D.isAfter(checkin, checkout)) {
    checkout = D.addDays(
      parseDateOrString(checkin),
      property.minNights
    ).toISOString();
  }

  if (!isMinimumNights(checkin, checkout, property.minNights)) {
    checkout = D.addDays(
      parseDateOrString(checkin),
      property.minNights
    ).toISOString();
  }

  // get updatet date range
  const dateRange = getNearestAvailabilityDates(
    [
      ...(property.availability?.data?.filter?.(
        (avble) => !avble.isAvailable
      ) || []),
      ...(property.reservations?.data?.filter?.(
        (rsv) => rsv.status !== "geannuleerd"
      ) || []),
    ],
    checkin,
    checkout,
    property.minNights
  );

  return {
    checkin: dateRange.checkin,
    checkout: dateRange.checkout,
  };
}

export function isPromise(value) {
  return Boolean(value && value instanceof Promise);
}

export const handleFunction = (fn, arg) => {
  try {
    return fn(arg);
  } catch (error) {
    throw new Error(
      `Error occured in [${fn.name || fn}] with args [${JSON.stringify(
        arg,
      )}] error: ${error.message}`,
    );
  }
};

export const pipe = (...fns) => {
  return (param) => {
    return fns.reduce((prevResult, currentFunction) => {
      return isPromise(prevResult)
        ? // eslint-disable-next-line no-console
          prevResult
            .then((promiseResult) =>
              handleFunction(currentFunction, promiseResult),
            )
            .catch(console.error)
        : handleFunction(currentFunction, prevResult);
    }, param);
  };
};

export const getRefToFile = (file) => {
  if (!file) {
    return '/anonymous_user.jpeg'
  }

  const [name, ext] = file.filename?.split?.(".") || [];
  return `/api/v1/uploads/${name}-xs.${ext}`;
}


export const getBlockedDays = (data) => {
  let blockedDays = [];

  if (Array.isArray(data)) {
    for (const date of data) {
      const range = moment.range(date.startDate, date.endDate);
      const acc = Array.from(range.by("day"));

      if (acc.length) {
        blockedDays = blockedDays.concat(acc);
      }
    }
  }

  return blockedDays
}

export const getQuery = (startDate, id) => ({
  sort: JSON.stringify([]),
  filter: JSON.stringify({
    startDate,
    endDate: D.addMonths(startDate, 1),
    property: id,
  }),
});