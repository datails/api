export const appConfig = {
  portalId: process.env.REACT_APP_PORTAL_ID,
  backendHost: process.env.REACT_APP_API_HOST || '',
}