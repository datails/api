# API

Add a createdBy property on endpoints to distinguish between creators. If you know who created a property, you know to whom the property in the admin portal should be visible.