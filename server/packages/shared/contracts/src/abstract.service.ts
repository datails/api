import * as N from '@nestjs/common';
import * as M from "mongoose";

import * as D from "@module/common";
import { FindAllByDateDto, FindAllDto, DeleteDto } from "./dto.contract";

export interface Portal extends Record<string, any> {
  _id?: string;
}

N.Injectable()
export class AbstractService<T, Y> {
  constructor(
    public readonly model: M.Model<any>,
    public readonly portal?: Portal
  ) {}

  async create(createDto: T, portalId: string = this.portal?._id): Promise<Y> {
    return D.create(this.model, createDto, portalId) as Promise<Y>;
  }

  async findById(id: string): Promise<Y> {
    return D.findById(this.model, id);
  }

  async findOne(query: Record<string, string>): Promise<Y> {
    return D.findOne(this.model, query);
  }

  async getSchema() {
    return this.model.schema.obj;
  }

  async findAll(
    findAllDto: FindAllDto,
    portalId = this.portal?._id,
    isSuperUser = this.portal?.isSuperUser
  ): Promise<{
    data: Y[];
    total: number;
  }> {
    return D.findAll(this.model, findAllDto, portalId, isSuperUser);
  }

  async findByDate(
    findAllByDateDto: FindAllByDateDto,
    portalId = this.portal?._id,
    isSuperUser = this.portal?.isSuperUser
  ): Promise<{
    data: Y[];
    total: number;
  }> {
    return D.findByDate(this.model, findAllByDateDto, portalId, isSuperUser);
  }

  async putMany(
    id: string,
    createDto: T,
    portalId = this.portal?._id
  ): Promise<Y> {
    return D.putMany(this.model, createDto, id, portalId) as Promise<Y>;
  }

  async putOne(
    id: string,
    createDto: T,
    portalId = this.portal?._id
  ): Promise<Y> {
    return D.putOne(this.model, createDto, id, portalId) as Promise<Y>;
  }

  async deleteMany(deleteDto: DeleteDto): Promise<string[]> {
    return D.deleteMany(this.model, deleteDto);
  }

  async deleteOne(id: string): Promise<Y> {
    return D.deleteOne(this.model, id);
  }
}
