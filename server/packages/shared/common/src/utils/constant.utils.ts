import { Resize } from './models.utils'

export const defaultSizes: Resize[] = [
    {
      size: 60,
      title: "xs",
    },
    {
      size: 120,
      title: "m",
    },
    {
      size: 240,
      title: "xl",
    },
  ];
  
  export const extendedSizes: Resize[] = [
    {
      size: 240,
      title: "xs",
    },
    {
      size: 480,
      title: "s",
    },
    {
      size: 640,
      title: "m",
    },
    {
      size: 960,
      title: "l",
    },
    {
      size: 1240,
      title: "xl",
    },
  ];