import { join } from "path";
import { FilesInterceptor, FileInterceptor } from "@nestjs/platform-express";
import { diskStorage } from "multer";

import { editFileName, imageFileFilter } from "./utils";

export const filesInterceptor = FilesInterceptor("files", 30, {
  storage: diskStorage({
    destination: join(process.cwd(), "files"),
    filename: editFileName,
  }),
  fileFilter: imageFileFilter,
});

export const fileInterceptor = FileInterceptor("file", {
  storage: diskStorage({
    destination: join(process.cwd(), "files"),
    filename: editFileName,
  }),
  fileFilter: imageFileFilter,
})