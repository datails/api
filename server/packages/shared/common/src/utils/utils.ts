import * as P from "path";
import { v4 as uuid } from "uuid";
import { Request } from "express";
import { UploadApiErrorResponse, UploadApiResponse, v2 } from 'cloudinary';
import toStream from 'buffer-to-stream';
import { defaultSizes, extendedSizes } from './constant.utils'

v2.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
})

export const uploadToCloudinary = (args: { file: Express.Multer.File, size: number, name: string, title: string, ext: string }): Promise<UploadApiResponse | UploadApiErrorResponse> => {
  return new Promise((resolve, reject) => {
    const upload = v2.uploader.upload_stream({
      transformation: [{ width: args.size }],
      filename_override: `${args.name}-${args.title}${args.ext}`,
      folder: process.env.CLOUDINARY_FOLDER,
    }, (error, result) => {
      if (error) return reject(error);
      resolve(result);
    });

    toStream(args.file.buffer).pipe(upload);
  });
}

export const handleResize = async <T extends Record<string,any>>(dto: T) => {
  if (dto.files && Array.isArray(dto.files)) {
    await Promise.all(
      dto.files.map(async (file) => resizeImages(file, extendedSizes))
    );
  }

  if (dto.file && Object.keys(dto.file).length) {
    await resizeImages(dto.file)
  }

}

export const resizeImages = (file: any, sizes = defaultSizes) => {
  if (!file) {
    return;
  }

  const [name] = file.filename.split(".");
  const ext = P.extname(file.filename);

  return Promise.all(
    sizes.map(async ({ size, title }) => uploadToCloudinary({ file, name, title, size, ext }))
  );
};

export function getFilePath(files: Array<Record<any, any>>) {
  return files.map((file) => {
    const [name, ext] = file.filename?.split?.(".") || [];
    return `/api/v1/uploads/${name}-xs.${ext}`;
  });
}

export const imageFileFilter = (_req: Request, file, callback: Function) => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return callback(new Error("Only image files are allowed!"), false);
  }
  callback(null, true);
};

export const editFileName = (_req: Request, file, callback: Function) => {
  const fileExtName = P.extname(file.originalname);
  const randomName = uuid();

  callback(null, `${randomName}${fileExtName}`);
};

export function getAccessTokenFromRequest(req: any = {}): string {
  return getToken(req.req || req);
}

export function getToken(request: any) {
  return (
    getTokenFromQuery(request) ||
    getTokenFromBearer(request) ||
    getTokenFromBody(request) ||
    ""
  );
}

export function getTokenFromQuery(request: any) {
  return request.query?.access_token;
}

export function getTokenFromBearer(request: any) {
  return request?.headers?.authorization?.split?.("Bearer ")?.[1];
}

export function getTokenFromBody(request: any) {
  request?.body?.access_token;
}

export const getApiHeader = (request: any) => {
  return request.headers['x-portal-id']
}