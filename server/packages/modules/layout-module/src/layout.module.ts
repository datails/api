import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { APP_MONGO_DB } from "@module/mongo-module";

import * as C from "./layout.controller";
import * as S from "./layout.service";
import * as I from "./layout.schema";

@Module({
  imports: [
    MongooseModule.forFeature(
      [{ name: I.Layout.name, schema: I.LayoutSchema }],
      APP_MONGO_DB
    ),
  ],
  controllers: [C.ClientController],
  providers: [S.LayoutService],
  exports: [S.LayoutService],
})
export class LayoutModule {}
