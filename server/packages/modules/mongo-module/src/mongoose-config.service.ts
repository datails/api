import {
  MongooseOptionsFactory,
  MongooseModuleOptions,
} from "@nestjs/mongoose";
import { Inject} from '@nestjs/common';
import { DYNAMIC_DATABASE_TABLE } from "../../switch-module/src/switch.constant";
import { ConfigService } from "@nestjs/config";
export class MongooseConfigService implements MongooseOptionsFactory {
  constructor(
    @Inject(DYNAMIC_DATABASE_TABLE)
    private readonly portal: any,
    private readonly configService: ConfigService
  ){}

  async createMongooseOptions(): Promise<MongooseModuleOptions> {    
    return {
      uri: `${this.configService.get('mongo.uri')}/booking`,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      w: "majority",

      // _id is a mongo id. Convert it to a string,
      // else it is an object
      dbName: typeof this.portal === 'string' ? this.portal : this.portal?._id?.toString(),
    };
  }
}
