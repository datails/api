import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { ConfigService } from "@nestjs/config";

import { MongooseConfigService } from "./mongoose-config.service";
import { APP_MONGO_DB, SSO_MONGO_DB } from "./mongoose.constants";

@Module({
  imports: [
    MongooseModule.forRootAsync({
      connectionName: APP_MONGO_DB,
      useClass: MongooseConfigService,
    }),
    MongooseModule.forRootAsync({
      connectionName: SSO_MONGO_DB,
      useFactory: (configService: ConfigService) => ({
        uri: `${configService.get('mongo.uri')}/sso`,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        w: "majority",
      }),
      inject: [ConfigService]
    }),
  ],
})
export class MongoModule {}
