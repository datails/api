import { Module, Global, DynamicModule } from "@nestjs/common";
import { createSwitchProviders } from "./switch.providers";
import { PortalModule} from '@module/portal-module'

@Global()
@Module({
  imports: [
    PortalModule
  ],
})
export class SwitchModule {
  public static forRoot(): DynamicModule {
    const providers = [createSwitchProviders()];

    return {
      module: SwitchModule,
      providers,
      exports: providers,
    };
  }
}
