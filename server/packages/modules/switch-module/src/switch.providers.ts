import { REQUEST } from "@nestjs/core";
import { getContext } from "./switch.service";
import { DYNAMIC_DATABASE_TABLE } from "./switch.constant";
import { PortalService } from "@module/portal-module";

export const createSwitchProviders = () => {
  return {
    provide: DYNAMIC_DATABASE_TABLE,
    useFactory: getContext,
    inject: [PortalService, REQUEST],
  };
};
