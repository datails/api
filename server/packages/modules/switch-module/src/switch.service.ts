import { UnauthorizedException } from "@nestjs/common";
import { decode } from "jsonwebtoken";
import { PortalService } from "@module/portal-module";
import * as C from "@module/common";

export const getContext = async (portalService: PortalService, req: any) => {
  const jwtToken = decode(
    C.getAccessTokenFromRequest(req)
  ) as any;

  const headerToken = C.getApiHeader(req);

  if (headerToken) {
    return headerToken
  }

  if (!jwtToken) {
    return
  }

  const portalId = jwtToken?.portalId?.find(Boolean);

  const portal = await portalService.findById(portalId)

  if (process.env.NODE_ENV === 'development') {
    return portal
  }

  if (!portal?.isActive) {
    throw new UnauthorizedException("Portal is not active!");
  }

  // @ts-ignore
  if (!portal?._id) {
    throw new UnauthorizedException(
      "Not a portalId available!",
    );
  }

  return portal;
}
