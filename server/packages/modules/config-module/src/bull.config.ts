import { registerAs } from "@nestjs/config";

export default registerAs("bull", () => ({
  name: process.env.QUEUE_NAME,
}));
