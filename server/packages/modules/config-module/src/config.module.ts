import { Module } from "@nestjs/common";
import * as C from "@nestjs/config";

import appConfig from "./app.config";
import bullConfig from "./bull.config";
import redisConfig from "./redis.config";
import mongoConfig from "./mongo.config";

@Module({
  imports: [
    C.ConfigModule.forRoot({
      isGlobal: true,
      load: [appConfig, bullConfig, mongoConfig, redisConfig],
    }),
  ],
})
export class ConfigModule {}
