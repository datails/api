import { Module, OnModuleInit, HttpModule } from "@nestjs/common";
import { threadId } from "worker_threads";

import * as E from "@module/email-module";
import * as P from "@module/property-module";
import * as U from "@module/user-module";
import * as M from "@module/mongo-module";
import * as C from "@module/config-module";
import * as S from "@module/switch-module";

@Module({
  imports: [
    S.SwitchModule.forRoot(),
    M.MongoModule,
    E.EmailModule,
    P.PropertyModule,
    U.UsersModule,
    HttpModule,
    C.ConfigModule,
  ],
  providers: [E.EmailService],
})
export class WorkerModule implements OnModuleInit {
  onModuleInit() {
    console.log("WORKER", threadId);
  }
}
