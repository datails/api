import { HttpService } from "@nestjs/common";
import * as NB from "@nestjs/bull";
import * as sgMail from "@sendgrid/mail";

import { Job } from "bull";

import * as P from "@module/public-module";
import * as U from "@module/user-module";
import * as C from "@module/client-module";
import * as R from "@module/availability-module";
import * as PS from "@module/property-module";

@NB.Processor(process.env.QUEUE_NAME)
export class EmailService {
  constructor(
    private httpService: HttpService,
    private readonly propertiesService: PS.PropertiesService,
    private readonly usersService: U.UsersService
  ) {
    sgMail.setApiKey(process.env.SMTP_KEY);
  }

  @NB.Process({
    name: "reservation.create",
    concurrency: 5,
  })
  public async createReservation(job: Job<P.CreatePublicReservationDTO>) {
    // TO DO:
    // initially this was linked to the owner of the property.
    // however, it should now be send to the owner of the platform.
    // I assume we do not even need the request scoped mongo connection
    // here for that, as we use the portal mongo connection, which is not
    // request scoped at all.
    // const property = await this.propertiesService.findById(
    //   job.data.reservation.property
    // );
    // const owners = await Promise.all(
    //   property.owner.map(async (person) => this.usersService.findById(person))
    // );
    // owners.forEach(async (owner) => {
    //   await this.createReservationEmail(
    //     owner,
    //     job.data.client,
    //     job.data.reservation
    //   );
    // });
  }

  private async createReservationEmail(
    owner: U.User,
    client: Partial<C.Client>,
    reservation: Partial<R.Reservation>
  ) {
    const sendgridEmailData = {
      propertyName: reservation.property,
      startDate: reservation.startDate,
      endDate: reservation.endDate,
      guests: reservation.children + reservation.adults,
      isPaid: reservation.isPaid ? "ja" : "nee",
      price: reservation.price,
    };

    await sgMail.send({
      to: client.email,
      from: "datails.smtp@gmail.com",
      subject: "Bevestiging reserveringsaanvraag",
      templateId: "d-e9390a359805410caa478a8589bfc6d3",
      dynamicTemplateData: {
        title: "Bevestiging reserveringsaanvraag",
        subtitle: "Bedankt voor uw reservering!",
        name: client.firstname,
        // @ts-ignore
        body: `Bedankt voor uw reservering! Uw reserveringsaanvraag met nummer ${reservation._id} moet nog worden bevestigd door de eigenaar. Hiervan ontvangt u zo spoedig mogelijk bericht.`,
        ...sendgridEmailData,
      },
    });

    await sgMail.send({
      to: owner.email,
      from: "datails.smtp@gmail.com",
      subject: "Nieuwe reserveringsaanvraag",
      templateId: "d-e9390a359805410caa478a8589bfc6d3",
      dynamicTemplateData: {
        title: "Nieuwe reserveringsaanvraag",
        subtitle: "Een nieuwe reservering!",
        name: owner.name,
        // @ts-ignore
        body: `Er is een nieuwe reserveringsaanvraag met nummer ${reservation._id} en deze moet nog door u worden bevestigd.`,
        ...sendgridEmailData,
      },
    });
  }

  @NB.Process({
    name: "reservation.update",
    concurrency: 5,
  })
  public async updateReservation(job: Job) {}

  @NB.Process({
    name: "reservation.delete",
    concurrency: 5,
  })
  public async deleteReservation(job: Job) {}

  @NB.OnQueueActive()
  public async onActive(job: Job) {}

  @NB.OnQueueCompleted()
  public async onCompleted(job: Job) {
    if (job.finishedOn) {
    }
  }

  @NB.OnQueueError()
  public async OnError(error: any) {}

  @NB.OnQueueFailed()
  public async OnFailed(job: Job, error: any) {}

  @NB.OnQueueRemoved()
  public async OnRemoved(job: Job) {}
}
