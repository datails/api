import * as SW from '@nestjs/swagger';
import * as V from "class-validator";

export class CreateClientDto {
  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly firstname: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly lastname: string;

  @SW.ApiProperty({ type: String, required: false })
  @V.IsOptional()
  @V.IsString()
  readonly insertion?: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly street: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly city: string;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsInt()
  readonly housenumber: number;

  @SW.ApiProperty({ type: String, required: false })
  @V.IsOptional()
  @V.IsString()
  readonly housenumberextension?: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly zipcode: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsEmail()
  readonly email: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsOptional()
  @V.IsString()
  readonly telephone?: string;
}
