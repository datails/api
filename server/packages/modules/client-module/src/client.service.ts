import * as N from "@nestjs/common";
import * as M from "@nestjs/mongoose";

import { Model } from "mongoose";

import * as U from "@shared/contracts";
import * as S from "@module/switch-module";

import * as I from "./client.schema";
import * as D from "./client.dto";

@N.Injectable()
export class ClientService extends U.AbstractService<
  D.CreateClientDto,
  I.Client
> {
  constructor(
    @M.InjectModel(I.Client.name)
    clientModel: Model<I.ClientDocument>,
    @N.Inject(S.DYNAMIC_DATABASE_TABLE)
    portal: U.Portal
  ) {
    super(clientModel, portal);
  }
}
