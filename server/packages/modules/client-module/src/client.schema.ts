import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type ClientDocument = Client & Document;

@Schema({
  autoCreate: true,
  id: true,
  timestamps: true,
})
export class Client {
  @Prop({ type: [String], required: true })
  portalId: string[];

  @Prop({ type: String, required: true })
  firstname: string;

  @Prop({ type: String, required: true })
  lastname: string;

  @Prop({ type: String })
  insertion?: string;

  @Prop({ type: String, required: true })
  street: string;

  @Prop({ type: String, required: true })
  city: string;

  @Prop({ type: Number, required: true })
  housenumber: number;

  @Prop({ type: String })
  housenumberextension?: string;

  @Prop({ type: String, required: true })
  zipcode: string;

  @Prop({ type: String, required: true })
  email: string;

  @Prop({ type: String })
  telephone?: string;
}

export const ClientSchema = SchemaFactory.createForClass(Client);
