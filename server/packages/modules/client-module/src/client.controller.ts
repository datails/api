import * as N from "@nestjs/common";
import * as SW from '@nestjs/swagger';

import * as C from "@shared/contracts";

import * as S from "./client.service";
import * as I from "./client.schema";
import * as D from "./client.dto";

@SW.ApiTags("Client")
@N.Controller("clients")
export class ClientController extends C.AbstractController<
  S.ClientService,
  D.CreateClientDto,
  I.Client
> {
  constructor(clientService: S.ClientService) {
    super(clientService);
  }
}
