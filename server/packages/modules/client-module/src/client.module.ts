import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { APP_MONGO_DB } from "@module/mongo-module";

import { ClientController } from "./client.controller";
import { ClientService } from "./client.service";
import { Client, ClientSchema } from "./client.schema";

@Module({
  imports: [
    MongooseModule.forFeature(
      [{ name: Client.name, schema: ClientSchema }],
      APP_MONGO_DB
    ),
  ],
  controllers: [ClientController],
  providers: [ClientService],
  exports: [ClientService],
})
export class ClientModule {}
