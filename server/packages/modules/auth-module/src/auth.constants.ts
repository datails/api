export const jwtConstants = {
  secret: process.env.JWT_SECRET,
};

export enum Role {
  User = "user",
  Admin = "admin",
  SuperUser = "superuser",
}
