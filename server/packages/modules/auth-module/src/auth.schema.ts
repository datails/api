import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type UserDocument = User & Document;

@Schema({
  autoCreate: true,
  id: true,
  timestamps: true,
})
export class User {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: String, required: true })
  username: string;

  @Prop({ type: String, required: true })
  password: string;

  @Prop({ type: String, required: true })
  email: string;

  @Prop({ type: [String], required: true })
  portalId: string[];

  @Prop({ type: Buffer, required: false })
  file?: Record<any, any>;

  @Prop({
    type: String,
    required: true,
    enum: ["admin", "user", "superuser"],
  })
  role: string;

  @Prop({ type: Boolean, required: true })
  isActive: boolean;
}

export const UserSchema = SchemaFactory.createForClass(User);
