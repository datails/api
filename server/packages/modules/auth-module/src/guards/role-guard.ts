import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Role } from "../auth.constants";
import { ROLES_KEY } from "../auth.decorators";

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const requiredRoles = this.reflector.getAllAndOverride<Role[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (!requiredRoles) {
      return true;
    }

    const { user } = context.switchToHttp().getRequest();

    // if it is the superuser, it has all
    // rights to change all platforms
    if (user.role?.includes("superuser")) {
      return true;
    }

    return requiredRoles.some((role) => user.role?.includes?.(role));
  }
}
