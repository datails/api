import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectModel } from "@nestjs/mongoose";

import * as bcrypt from "bcrypt";
import { Model } from "mongoose";

import * as S from "./auth.schema";

interface ISignedUser {
  email: string;
  portalId: string[];
  userId: string;
  username: string;
  role: string;
  isActive: boolean;
}

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(S.User.name)
    private readonly userModel: Model<S.UserDocument>,
    private readonly jwtService: JwtService
  ) {}

  async validateUser(username: string, pass: string): Promise<ISignedUser> {    
    const user = await this.userModel.findOne({ username }).lean().exec();
    if (user && (await bcrypt.compare(pass, user.password))) {
      // @ts-ignore
      const { email, _id, username, role, isActive, portalId } = user;

      return {
        email,
        userId: _id,
        username,
        role,
        isActive,
        portalId,
      };
    }
    return null;
  }

  async login(user: ISignedUser) {
    const payload = {
      username: user.username,
      sub: user.userId,
      role: user.role,
      portalId: user.portalId,
    };

    return {
      token: this.jwtService.sign(payload),
    };
  }
}
