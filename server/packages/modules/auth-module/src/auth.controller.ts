import * as N from "@nestjs/common";
import * as SW from '@nestjs/swagger';
import { AuthService } from "./auth.service";
import { LocalAuthGuard } from "./guards";
import { Public } from "./auth.decorators";

@SW.ApiTags("Auth")
@N.Controller("auth")
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @SW.ApiResponse({
    status: 201,
    description: 'Login controller.',
  })
  @Public()
  @N.UseGuards(LocalAuthGuard)
  @N.Post("/login")
  @N.Header("Content-Type", "application/json")
  async login(@N.Request() req: any) {
    return this.authService.login(req.user);
  }
}
