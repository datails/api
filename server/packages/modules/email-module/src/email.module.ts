import { BullModule } from "@nestjs/bull";
import { Module, HttpModule, Global } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";

import { EmailService } from "./email.service";

@Global()
@Module({
  imports: [
    HttpModule,
    BullModule.registerQueueAsync({
      name: process.env.QUEUE_NAME,
      useFactory: (config: ConfigService) => ({
        name: config.get<string>("bull.name"),
        defaultJobOptions: {
          attempts: 15,
          backoff: 3600000,
          delay: 1000,
          timeout: 3000,
          removeOnFail: true,
          removeOnComplete: true,
          stackTraceLimit: 3,
        },
        redis: {
          host: config.get<string>("redis.host"),
          port: config.get<number>("redis.port"),
          showFriendlyErrorStack: true,
          keyPrefix: config.get<string>("redis.prefix"),
        },
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [EmailService],
  exports: [EmailService, BullModule],
})
export class EmailModule {}
