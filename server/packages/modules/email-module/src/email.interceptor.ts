import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { InjectQueue } from "@nestjs/bull";
import { Queue } from "bull";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

import { EMAIL_TYPES } from "./email-interceptor.enum";
import { EMAIL_KEY } from "./email.decorator";

interface Response<T> {
  data: T;
}

@Injectable()
export class EmailInterceptor<T> implements NestInterceptor<T, Response<T>> {
  constructor(
    @InjectQueue(process.env.QUEUE_NAME) private readonly emailQueue: Queue,

    private readonly reflector: Reflector
  ) {}

  intercept(
    context: ExecutionContext,
    next: CallHandler
  ): Observable<Response<T>> {
    const { body } = context.switchToHttp().getRequest();

    const emailTypes =
      this.reflector.getAllAndOverride<EMAIL_TYPES[]>(EMAIL_KEY, [
        context.getHandler(),
        context.getClass(),
      ]) || [];

    return next.handle().pipe(
      tap(async (data) => {
        const resp = context.switchToHttp().getResponse();

        // only send email if the request was successfull
        if (resp.statusCode >= 200 && resp.statusCode < 300) {
          const promises = emailTypes.map(async (type) =>
            this.emailQueue.add(type, data)
          );
          await Promise.all(promises);
        }
      })
    );
  }
}
