import { Injectable, HttpService, ForbiddenException } from "@nestjs/common";

@Injectable()
export class EmailService {
  constructor(private readonly httpService: HttpService) {}

  public async validateRecaptcha(
    recaptchaValue: string,
    ip: string
  ): Promise<boolean> {
    const { data } = await this.httpService
      .post(
        `https://www.google.com/recaptcha/api/siteverify?response=${recaptchaValue}&secret=6LcZVo0aAAAAAMjokRvDhpuBh8uw4nKJPxGm2bMZ&remoteip=${ip}`
      )
      .toPromise();

    if (!data.success) {
      throw new ForbiddenException();
    }

    return data.success;
  }
}
