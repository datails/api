import { Module, Global } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { SSO_MONGO_DB } from "@module/mongo-module";

import * as C from "./portal.controller";
import * as S from "./portal.service";
import * as I from "./portal.schema";

@Module({
  imports: [
    MongooseModule.forFeature(
      [{ name: I.Portal.name, schema: I.PortalSchema }],
      SSO_MONGO_DB
    ),
  ],
  controllers: [C.PortalController],
  providers: [S.PortalService],
  exports: [S.PortalService],
})
export class PortalModule {}
