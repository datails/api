import * as N from "@nestjs/common";
import * as SW from '@nestjs/swagger';

import * as A from "@module/auth-module";
import * as C from "@shared/contracts";
import * as S from "@module/switch-module";
import * as U from "@shared/contracts";

import { PortalService } from "./portal.service";
import * as D from "./portal.dto";
import * as I from "./portal.schema";

@SW.ApiTags("Portal")
@N.Controller("portals")
export class PortalController {
  constructor(
    private readonly portalService: PortalService,
    @N.Inject(S.DYNAMIC_DATABASE_TABLE)
    private readonly portal: U.Portal
  ) {}

  @N.Post()
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.SuperUser)
  async create(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createPortalDto: D.CreatePortalDto,
    @N.UploadedFile() logo: Record<any, any>
  ) {
    return this.portalService.create(createPortalDto, this.portal?._id);
  }

  @N.Get("/many")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.SuperUser)
  async findAll(
    @N.Query(new N.ValidationPipe({ transform: true }))
    findAllPortalsDto: C.FindAllDto
  ): Promise<{ data: I.Portal[]; total: number }> {
    return this.portalService.findAll(findAllPortalsDto, this.portal?._id, this.portal?.isSuperUser);
  }

  @N.Get("/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin, A.Role.User)
  async findById(@N.Param() { id }): Promise<I.Portal> {
    return this.portalService.findById(id);
  }

  @N.Put("/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin)
  async putOne(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createPortalDto: D.CreatePortalDto,
    @N.Param() { id },
    @N.UploadedFile() logo: Record<any, any>
  ): Promise<I.Portal> {
    const resp = await this.portalService.findById(id);

    return this.portalService.putOne(id, {
      ...resp,
      ...createPortalDto,
    },
      this.portal?._id);
  }

  @N.Put("/many/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.SuperUser)
  async putMany(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createPortalDto: D.CreatePortalDto,
    @N.Param() { id },
    @N.UploadedFile() logo: Record<any, any>
  ): Promise<I.Portal> {
    return this.portalService.putMany(id, createPortalDto, this.portal?._id);
  }

  @N.Delete("/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.SuperUser)
  async deleteOne(@N.Param() { id }): Promise<I.Portal> {
    return this.portalService.deleteOne(id);
  }

  @N.Delete("/many/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.SuperUser)
  async deleteMany(
    @N.Query(new N.ValidationPipe({ transform: true }))
    deletePortalsDto: C.DeleteDto
  ): Promise<string[]> {
    return this.portalService.deleteMany(deletePortalsDto);
  }
}
