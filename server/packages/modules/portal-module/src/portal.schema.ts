import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type PortalDocument = Portal & Document;

@Schema({
  autoCreate: true,
  id: true,
  timestamps: true,
})
export class Portal {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: String, required: false })
  url?: string;

  @Prop({ type: String, required: false })
  ownerId?: string;

  @Prop({ type: String, required: false })
  iban?: string;

  @Prop({ type: Boolean, required: true })
  isActive: Boolean;
}

export const PortalSchema = SchemaFactory.createForClass(Portal);
