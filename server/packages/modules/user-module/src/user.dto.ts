import * as SW from '@nestjs/swagger';
import * as V from "class-validator";
import { Transform } from "class-transformer";

import * as C from "@shared/contracts";

export class CreateUserDTO {
  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsEmail()
  readonly email: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly name: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly username: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly password: string;

  @SW.ApiProperty({ type: [String], required: true })
  @V.IsNotEmpty()
  @V.IsString({ each: true })
  @Transform(C.toArray)
  readonly portalId: string[];

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly role: string;

  @SW.ApiProperty({ type: Object, required: false })
  @V.IsOptional()
  @Transform(C.toFile)
  file?: Record<any, any>;

  @SW.ApiProperty({ type: Boolean, required: true })
  @V.IsNotEmpty()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly isActive: boolean;
}
