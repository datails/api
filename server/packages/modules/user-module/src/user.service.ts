import * as N from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";

import * as bcrypt from "bcrypt";
import { Model } from "mongoose";

import * as C from "@shared/contracts";
import { DYNAMIC_DATABASE_TABLE } from "@module/switch-module";

import * as S from "./user.schema";
import * as D from "./user.dto";

@N.Injectable()
export class UsersService extends C.AbstractService<D.CreateUserDTO, S.User> {
  private SALT = 10;

  constructor(
    @InjectModel(S.User.name)
    userModel: Model<S.UserDocument>,
    @N.Inject(DYNAMIC_DATABASE_TABLE)
    public portal: C.Portal
  ) {
    super(userModel, portal);
  }

  async isUserCreateAllowed(createUserDto: D.CreateUserDTO) {
    const isUserNameExist = await this.findOne({
      username: createUserDto.username,
    });
    const isUserEmailExist = await this.findOne({ email: createUserDto.email });

    if (isUserNameExist?.username === createUserDto.username) {
      throw new N.ConflictException("Gebruikersnaam bestaat al!");
    }

    if (isUserEmailExist?.email === createUserDto.email) {
      throw new N.ConflictException("Emailadres bestaat al!");
    }

    if (createUserDto.role.includes("superuser") && !this.portal?.isSuperUser) {
      throw new N.ForbiddenException(
        "Superusers kunnen alleen door superusers worden aangemaakt!"
      );
    }

    return true;
  }

  async create(createUserDTO: D.CreateUserDTO): Promise<S.User> {
    await this.isUserCreateAllowed(createUserDTO);

    return super.create({
      ...createUserDTO,
      password: await bcrypt.hash(createUserDTO.password, this.SALT),
    },
    // @ts-ignore
      createUserDTO.portalId[0]._id.toString()
    );
  }
}
