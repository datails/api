import * as N from "@nestjs/common";
import { isBefore } from "date-fns";
import * as D from "./availability/availability.dto";

export const isAvailable = async (
  createDto: D.CreateReservationDto | D.CreateAvailabilityDto,
  fn: any,
  findByDate: any
) => {
  const availability = await fn({
    range: [],
    sort: [],
    filter: {
      startDate: createDto.startDate,
      endDate: createDto.endDate,
      property: createDto.property,
    },
  });

  const { data } = await findByDate({
    range: [],
    sort: [],
    filter: {
      startDate: createDto.startDate,
      endDate: createDto.endDate,
      property: createDto.property,
    },
  });

  if (isBefore(new Date(createDto.endDate), new Date(createDto.startDate))) {
    throw new N.ConflictException(
      "Startdatum kan niet groter zijn dan einddatum!"
    );
  }

  if (isBefore(new Date(createDto.startDate), new Date())) {
    throw new N.ConflictException(
      "Startdatum kan niet in het verleden liggen!"
    );
  }

  // @ts-ignore
  if (data.length && data.some((event) => event?.isAvailable === false)) {
    throw new N.ConflictException("Er is een boeking in deze periode!");
  }

  if (
    availability.data.length &&
    availability.data.some((obj) => obj?.isAvailable === false)
  ) {
    throw new N.ConflictException(
      "Er is geen beschikbaarheid in deze periode!"
    );
  }

  return true;
};
