import * as SW from '@nestjs/swagger';
import * as V from "class-validator";

export enum StatusType {
  aanvraag = "aanvraag",
  geaccepteerd = "geaccepteerd",
  geannuleerd = "geannuleerd",
}

export class CreateReservationDto {
  @SW.ApiProperty({ type: String, required: false })
  @V.IsOptional()
  @V.IsString()
  readonly client?: string;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsNumber()
  readonly price: number;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly property: string;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsNumber()
  readonly adults: number;

  @SW.ApiProperty({ type: Number, required: false })
  @V.IsOptional()
  @V.IsNumber()
  readonly children: number;

  @SW.ApiProperty({ type: Date, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly startDate: Date;

  @SW.ApiProperty({ type: Date, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly endDate: Date;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly status: keyof typeof StatusType;

  @SW.ApiProperty({ type: Boolean, required: true })
  @V.IsNotEmpty()
  @V.IsBoolean()
  readonly isPaid: boolean;

  @SW.ApiProperty({ type: String, required: false })
  @V.IsOptional()
  @V.IsString()
  readonly recaptchaValue?: string;

  @SW.ApiProperty({ type: String, required: false })
  @V.IsOptional()
  @V.IsString()
  readonly "g-recaptcha-response"?: string;
}
