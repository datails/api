import * as N from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";

import { Model } from "mongoose";

import * as C from "@module/common";
import { AbstractService, FindAllDto, DeleteDto, FindAllByDateDto } from '@shared/contracts';

import { DYNAMIC_DATABASE_TABLE } from "@module/switch-module";
import { PropertiesService } from "@module/property-module";

import * as S from "./availability.schema";
import * as D from "./availability.dto";

import * as U from "../utils";

/**
 *
 * NOTE:
 *
 * due to a bug that we cannot use scoped requests
 * and forwardRef 2 ways, we had to merge the availability
 * and reservation services. Therefor the reservation service
 * contains all the availability services as well
 */

@N.Injectable()
export class ReservationService extends AbstractService<
  D.CreateReservationDto,
  S.Reservation
> {
  constructor(
    @InjectModel(S.Reservation.name)
    private readonly reservationModel: Model<S.ReservationDocument>,
    @InjectModel(S.Availability.name)
    private availabilityModel: Model<S.AvailabilityDocument>,
    @N.Inject(DYNAMIC_DATABASE_TABLE)
    public portal: any,
    private propertyService: PropertiesService
  ) {
    super(reservationModel, portal);
  }

  async create(
    createReservationDto: D.CreateReservationDto
  ): Promise<S.Reservation> {
    const property = await this.propertyService.findById(
      createReservationDto.property
    );

    await U.isAvailable(
      createReservationDto,
      this.findAllAvailability.bind(this),
      (findByDateDto: any, portalId = property.portalId) => this.findByDate(findByDateDto, portalId)
    );

    return super.create(createReservationDto, property.portalId);
  }

  async putOne(
    id: string,
    createReservationDto: D.CreateReservationDto
  ): Promise<S.Reservation> {
    await U.isAvailable(createReservationDto, this.findAllAvailability.bind(this), this.findByDate.bind(this));
    return super.putOne(id, createReservationDto);
  }

  async putMany(
    id: string,
    createReservationDto: D.CreateReservationDto
  ): Promise<S.Reservation> {
    await U.isAvailable(createReservationDto, this.findAllAvailability.bind(this), this.findByDate.bind(this));

    return super.putMany(id, createReservationDto);
  }

  /**
   *
   * ================
   * AVAILABILITY METHODS
   * ================
   *
   */

  async createAvailability(
    createAvailabilityDto: D.CreateAvailabilityDto
  ): Promise<S.Availability> {
    await U.isAvailable(createAvailabilityDto, this.findAll.bind(this), this.findByDate.bind(this));
    const property = await this.propertyService.findById(
      createAvailabilityDto.property
    );

    return C.create(
      this.availabilityModel,
      // @ts-ignore
      createAvailabilityDto,
      property.portalId
    );
  }

  async findByIdAvailability(id: string): Promise<S.Availability> {
    return C.findById(this.availabilityModel, id);
  }

  async findOneAvailability(name: string): Promise<S.Availability> {
    return this.availabilityModel.findOne({ username: name }).exec();
  }

  async findByDateAvailability(
    findByDateDto: FindAllByDateDto
  ): Promise<{
    data: S.Availability[];
    total: number;
  }> {
    return C.findByDate(
      this.availabilityModel,
      findByDateDto,
      this.portal?._id,
      this.portal?.isSuperUser
    );
  }

  async findAllAvailability(
    findAllDto: FindAllDto
  ): Promise<{
    data: S.Availability[];
    total: number;
  }> {
    return C.findAll(
      this.availabilityModel,
      findAllDto,
      this.portal?._id,
      this.portal?.isSuperUser
    );
  }

  async putManyAvailability(
    id: string,
    updateAvailabilityDto: D.CreateAvailabilityDto
  ): Promise<S.Availability> {
    await U.isAvailable(updateAvailabilityDto, this.findAll.bind(this), this.findByDate.bind(this));

    return C.putMany(
      this.availabilityModel,
      //@ts-ignore
      updateAvailabilityDto,
      id,
      this.portal?._id
    );
  }

  async putOneAvailability(
    id: string,
    updateAvailabilityDto: D.CreateAvailabilityDto
  ): Promise<S.Availability> {
    await U.isAvailable(updateAvailabilityDto, this.findAll.bind(this), this.findByDate.bind(this));

    return C.putOne(
      this.reservationModel,
      //@ts-ignore
      updateAvailabilityDto,
      id,
      this.portal?._id
    );
  }

  async deleteManyAvailability(
    deleteAvailabilityDto: DeleteDto
  ): Promise<string[]> {
    return C.deleteMany(this.availabilityModel, deleteAvailabilityDto);
  }

  async deleteOneAvailability(id: string): Promise<S.Availability> {
    return C.deleteOne(this.availabilityModel, id);
  }
}
