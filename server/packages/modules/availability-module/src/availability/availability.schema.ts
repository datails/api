import * as S from "@nestjs/mongoose";
import * as SW from '@nestjs/swagger';
import * as M from "mongoose";

export * from "../reservation/reservation.schema";

export type AvailabilityDocument = Availability & M.Document;

@S.Schema({
  autoCreate: true,
  id: true,
  timestamps: true,
})
export class Availability {
  @SW.ApiProperty({ type: String, required: true })
  @S.Prop({ type: String, required: true })
  portalId: string;

  @SW.ApiProperty({ type: String, required: false })
  @S.Prop({ type: String })
  name: string;

  @SW.ApiProperty({ type: String, required: true })
  @S.Prop({ type: String, required: true })
  property: string;

  @SW.ApiProperty({ type: Date, required: true })
  @S.Prop({ type: Date, required: true })
  startDate: Date;

  @SW.ApiProperty({ type: Date, required: true })
  @S.Prop({ type: Date, required: true })
  endDate: Date;

  @SW.ApiProperty({ type: Number, required: true })
  @S.Prop({ type: Number })
  price: number;

  @SW.ApiProperty({ type: Boolean, required: true })
  @S.Prop({ type: Boolean, required: true })
  isAvailable: boolean;
}

export const AvailabilitySchema = S.SchemaFactory.createForClass(Availability);
