import * as N from "@nestjs/common";
import * as SW from '@nestjs/swagger';

import * as C from "@shared/contracts";
import * as A from "@module/auth-module";

import { ReservationService } from "./availability.service";
import { Availability } from "./availability.schema";

import * as D from "./availability.dto";

@SW.ApiTags("Availability")
@N.Controller("availability")
export class AvailabilityController {
  constructor(private readonly availabilityService: ReservationService) {}

  @N.Post()
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin)
  async create(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createAvailabilityDto: D.CreateAvailabilityDto
  ) {
    return this.availabilityService.createAvailability(createAvailabilityDto);
  }

  @N.Get("/many")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin)
  async findAll(
    @N.Query(new N.ValidationPipe({ transform: true }))
    findAllDto: C.FindAllDto
  ): Promise<{ data: Availability[]; total: number }> {
    return this.availabilityService.findAllAvailability(findAllDto);
  }

  @N.Get("/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin, A.Role.User)
  async findById(@N.Param() { id }): Promise<Availability> {
    return this.availabilityService.findByIdAvailability(id);
  }

  @N.Put("/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin)
  async putOne(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createReservationDto: D.CreateAvailabilityDto,
    @N.Param() { id }
  ): Promise<Availability> {
    return this.availabilityService.putOneAvailability(
      id,
      createReservationDto
    );
  }

  @N.Delete("/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin)
  async deleteOne(@N.Param() { id }): Promise<Availability> {
    return this.availabilityService.deleteOneAvailability(id);
  }

  @N.Delete("/many/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin)
  async deleteMany(
    @N.Query(new N.ValidationPipe({ transform: true }))
    deleteDto: C.DeleteDto
  ): Promise<string[]> {
    return this.availabilityService.deleteManyAvailability(deleteDto);
  }
}
