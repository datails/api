import * as SW from '@nestjs/swagger';
import * as V from "class-validator";

export * from "../reservation/reservation.dto";

export class CreateAvailabilityDto {
  @SW.ApiProperty({ type: String, required: false })
  @V.IsOptional()
  @V.IsString()
  readonly name: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly property: string;

  @SW.ApiProperty({ type: Date, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly startDate: Date;

  @SW.ApiProperty({ type: Date, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly endDate: Date;

  @SW.ApiProperty({ type: Number, required: false })
  @V.IsOptional()
  @V.IsInt()
  readonly price: number;

  @SW.ApiProperty({ type: Boolean, required: true })
  @V.IsNotEmpty()
  @V.IsBoolean()
  readonly isAvailable: boolean;
}
