import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { APP_MONGO_DB } from "@module/mongo-module";

import { PropertiesController } from "./property.controller";
import { PropertiesService } from "./property.service";
import { Property, PropertySchema } from "./property.schema";

@Module({
  imports: [
    MongooseModule.forFeature(
      [{ name: Property.name, schema: PropertySchema }],
      APP_MONGO_DB
    ),
  ],
  controllers: [PropertiesController],
  providers: [PropertiesService],
  exports: [PropertiesService],
})
export class PropertyModule {}
