export const sizes = [
  {
    size: 240,
    title: "xs",
  },
  {
    size: 480,
    title: "s",
  },
  {
    size: 640,
    title: "m",
  },
  {
    size: 960,
    title: "l",
  },
  {
    size: 1240,
    title: "xl",
  },
];
