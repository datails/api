import * as N from "@nestjs/common";
import * as M from "@nestjs/mongoose";

import { Model } from "mongoose";

import * as S from "@module/switch-module";
import * as C from '@shared/contracts';

import * as I from "./property.schema";
import * as D from "./property.dto";

@N.Injectable()
export class PropertiesService extends C.AbstractService<
  D.CreatePropertyDto,
  I.Property
> {
  constructor(
    @M.InjectModel(I.Property.name)
    propertyModel: Model<I.PropertyDocument>,
    @N.Inject(S.DYNAMIC_DATABASE_TABLE)
    portal: C.Portal
  ) {
    super(propertyModel, portal);
  }
}
