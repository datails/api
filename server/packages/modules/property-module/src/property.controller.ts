import * as N from "@nestjs/common";
import * as SW from '@nestjs/swagger';

import * as A from "@module/auth-module";
import * as C from '@shared/contracts';
import * as U from "@module/common";

import { CreatePropertyDto } from "./property.dto";
import { PropertiesService } from "./property.service";
import { Property } from "./property.schema";

@SW.ApiTags("Property")
@N.Controller("properties")
export class PropertiesController {
  constructor(private readonly propertiesService: PropertiesService) {}

  @N.Post()
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.SuperUser, A.Role.Admin)
  @N.UseInterceptors(U.filesInterceptor)
  async create(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createPropertyDto: CreatePropertyDto,
    @N.UploadedFiles() files: Array<Record<any, any>>
  ) {
    return this.propertiesService.create({
      ...createPropertyDto,
      files,
    });
  }

  @N.Get("/many")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.SuperUser, A.Role.Admin)
  async findAll(
    @N.Query(new N.ValidationPipe({ transform: true }))
    findAllDto: C.FindAllDto
  ): Promise<{ data: Property[]; total: number }> {
    return this.propertiesService.findAll(findAllDto);
  }

  @N.Get("/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.SuperUser, A.Role.Admin, A.Role.User)
  async findById(@N.Param() { id }): Promise<Property> {
    return this.propertiesService.findById(id);
  }

  @N.Put("/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.SuperUser, A.Role.Admin)
  @N.UseInterceptors(U.filesInterceptor)
  async putOne(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createReservationDto: CreatePropertyDto,
    @N.Param() { id },
    @N.UploadedFiles() files: Array<Record<any, any>>
  ): Promise<Property> {
    return this.propertiesService.putOne(id, {
      ...createReservationDto,
      files: [...(createReservationDto?.files || []), ...files],
    });
  }

  @N.Put("/many/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.SuperUser, A.Role.Admin)
  @N.UseInterceptors(U.filesInterceptor)
  async putMany(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createPropertyDto: CreatePropertyDto,
    @N.Param() { id },
    @N.UploadedFiles() files: Array<Record<any, any>>
  ): Promise<Property> {
    return this.propertiesService.putMany(id, {
      ...createPropertyDto,
      files,
    });
  }

  @N.Delete("/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.SuperUser, A.Role.Admin)
  async deleteOne(@N.Param() { id }): Promise<Property> {
    return this.propertiesService.deleteOne(id);
  }

  @N.Delete("/many/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.SuperUser, A.Role.Admin)
  async deleteMany(
    @N.Query(new N.ValidationPipe({ transform: true }))
    deleteDto: C.DeleteDto
  ): Promise<string[]> {
    return this.propertiesService.deleteMany(deleteDto);
  }
}
