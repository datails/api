import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import * as mongoose from "mongoose";

export type PropertyDocument = Property & mongoose.Document;

export interface IBasicServices {
  [service: string]: string;
}

@Schema({
  autoCreate: true,
  id: true,
  timestamps: true,
})
export class Property {
  @Prop({ type: String, required: true })
  portalId: string;

  @Prop({ type: String, required: true })
  title: string;

  @Prop({ type: String, required: true })
  description: string;

  @Prop({ type: mongoose.Schema.Types.Mixed, required: true })
  files: Array<Record<any, any>>;

  @Prop({ type: String, required: true })
  rules: string;

  @Prop({ type: Number, required: true })
  price: number;

  @Prop({ type: Number, required: true })
  minNights: number;

  @Prop({ type: Number, required: true })
  servicePrice: number;

  @Prop({ type: Boolean, default: false })
  isVATIncluded: boolean;

  @Prop({ type: Number, required: true })
  numberOfDaysBeforeCancel: number;

  @Prop({ type: Number, required: true })
  persons: number;

  @Prop({ type: Number, required: true })
  squareMeters: number;

  @Prop({ type: Number, required: true })
  chambers: number;

  @Prop({ type: Number, required: true })
  beds: number;

  @Prop({ type: Number, required: true })
  bathrooms: number;

  @Prop({ type: String, required: true })
  checkin: string;

  @Prop({ type: String, required: true })
  checkout: string;

  @Prop({ type: Number, required: true })
  lat: number;

  @Prop({ type: Number, required: true })
  lng: number;

  @Prop({ type: String, required: true })
  city: string;

  @Prop({ type: String, required: true })
  street: string;

  @Prop({ type: Number, required: true })
  housenumber: number;

  @Prop({ type: String, required: false })
  housenumber_extension: string;

  @Prop({ type: String, required: true })
  zipcode: string;

  @Prop({ type: Boolean, default: false })
  isActive: boolean;

  @Prop({ type: Boolean, default: false })
  isHandicapped: boolean;

  @Prop({ type: Boolean, default: false })
  isSmokingProperty: boolean;

  @Prop({ type: Boolean, default: false })
  isFitForBabies: boolean;

  @Prop({ type: Boolean, default: false })
  isFitForAnimals: boolean;

  @Prop({ type: Boolean, default: false })
  hasTelevision: boolean;

  @Prop({ type: Boolean, default: false })
  hasWarmWater: boolean;

  @Prop({ type: Boolean, default: false })
  hasHeating: boolean;

  @Prop({ type: Boolean, default: false })
  hasFireplace: boolean;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  basicServices: IBasicServices;

  @Prop({ type: Boolean, default: false })
  hasWifi: boolean;

  @Prop({ type: [String], required: true })
  parking: string[];

  @Prop({ type: Boolean, default: false })
  hasStove: boolean;

  @Prop({ type: Boolean, default: false })
  hasPlatesAndCutlery: boolean;

  @Prop({ type: Boolean, default: false })
  hasMicrowave: boolean;

  @Prop({ type: Boolean, default: false })
  hasCoffeeMachine: boolean;

  @Prop({ type: Boolean, default: false })
  hasCookingSupplies: boolean;

  @Prop({ type: Boolean, default: false })
  hasKitchen: boolean;

  @Prop({ type: Boolean, default: false })
  hasFridge: boolean;

  @Prop({ type: Boolean, default: false })
  hasOwnEntrance: boolean;

  @Prop({ type: Boolean, default: true })
  isCleaningBeforeLeave: boolean;

  @Prop({ type: Boolean, default: false })
  hasClothesHangers: boolean;

  @Prop({ type: Boolean, default: false })
  hasBedding: boolean;

  @Prop({ type: Boolean, default: false })
  hasShowerGel: boolean;

  @Prop({ type: Boolean, default: false })
  hasShampoo: boolean;

  @Prop({ type: Boolean, default: false })
  hasGarden: boolean;

  @Prop({ type: Boolean, default: false })
  hasBarn: boolean;

  @Prop({ type: Boolean, default: false })
  hasSmokeAlarm: boolean;

  @Prop({ type: Boolean, default: false })
  hasCarbonMonoxideDetector: boolean;

  @Prop({ type: Boolean, default: false })
  hasHairDryer: boolean;

  @Prop({ type: Boolean, default: false })
  hasAirconditioning: boolean;

  @Prop({ type: Boolean, default: false })
  hasWachingMachine: boolean;
}

export const PropertySchema = SchemaFactory.createForClass(Property);
