import * as N from "@nestjs/common";
import * as SW from '@nestjs/swagger';

import * as E from "@module/email-module";
import * as A from "@module/auth-module";
import * as P from "@module/property-module";
import * as U from "@module/user-module";
import * as R from "@module/availability-module";
import * as CS from "@module/client-module";
import * as PS from "@module/portal-module";
import * as C from "@shared/contracts";
import { getFilePath } from "@module/common"

import * as D from "./public.dto";

@SW.ApiTags("Public")
@N.Controller("public")
export class PublicController {
  constructor(
    private readonly propertiesService: P.PropertiesService,
    private readonly usersService: U.UsersService,
    private readonly reservationsService: R.ReservationService,
    private readonly clientService: CS.ClientService,
    private readonly portalService: PS.PortalService
  ) {}

  @N.Get("/property-schema")
  @N.Header("Content-Type", "application/json")
  @A.Public()
  async getPropertySchema() {
    return this.propertiesService.getSchema();
  }

  @N.Get("/property-list")
  @N.Header("Content-Type", "application/json")
  @A.Public()
  async findManyProperties(
    @N.Query(new N.ValidationPipe({ transform: true }))
    findAllDto: C.FindAllDto,
  ): Promise<{ data: Array<Partial<P.Property>>; total: number }> {
    const properties = await this.propertiesService.findAll(findAllDto);

    return {
      data: properties.data.map((property) => {
        const { files, ...rest } = property;
        return {
          ...rest,
          images: getFilePath(files),
        };
      }),
      total: properties.total,
    };
  }

  @N.Get("/property/:id")
  @N.Header("Content-Type", "application/json")
  @A.Public()
  async findOneProperty(@N.Param() { id }): Promise<P.Property> {
    return this.propertiesService.findById(id);
  }

  @N.Get("/portal/:id")
  @N.Header("Content-Type", "application/json")
  @A.Public()
  async findOnePortal(@N.Param() { id }): Promise<PS.Portal> {
    return this.portalService.findById(id);
  }

  @N.Get("/owner/:id")
  @N.Header("Content-Type", "application/json")
  @A.Public()
  async findOwner(@N.Param() { id }): Promise<{ name: string; file: any }> {
    const { name, file } = await this.usersService.findById(id);
    return { name, file };
  }

  @N.Get("/availability")
  @N.Header("Content-Type", "application/json")
  @A.Public()
  async findAvailabilityOfOneProperty(
    @N.Query(new N.ValidationPipe({ transform: true }))
    findAllByDate: C.FindAllByDateDto
  ): Promise<{ data: R.Availability[]; total: number }> {
    return this.reservationsService.findByDateAvailability(findAllByDate);
  }

  @N.Get("/reservations")
  @N.Header("Content-Type", "application/json")
  @A.Public()
  async findAll(
    @N.Query(new N.ValidationPipe({ transform: true }))
    findAllByDate: C.FindAllByDateDto
  ): Promise<{ data: { startDate: Date; endDate: Date }[]; total: number }> {
    const resp = await this.reservationsService.findByDate(findAllByDate);

    const transformedResp = resp.data.map(({ startDate, endDate }) => ({
      startDate,
      endDate,
    }));

    return {
      ...resp,
      data: transformedResp,
    };
  }

  @N.Get("/reservation/:id")
  @N.Header("Content-Type", "application/json")
  @A.Public()
  async findOneReservation(
    @N.Param(new N.ValidationPipe({ transform: true }))
    { id }: any
  ): Promise<R.Reservation> {
    return this.reservationsService.findById(id);
  }

  @N.Post("/create-reservation")
  @N.Header("Content-Type", "application/json")
  @A.Public()
  @E.Email(E.EMAIL_TYPES.RESERVATION_CREATE)
  @N.UseGuards(A.RecaptchaGuard)
  @N.UseInterceptors(E.EmailInterceptor)
  async createReservation(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createReservationDto: D.CreatePublicReservationDTO
  ): Promise<{ client: CS.Client; reservation: R.Reservation }> {
    const client = (await this.clientService
      .create(createReservationDto.client)
      .catch(console.error)) as CS.Client;

    const reservation = (await this.reservationsService
      .create({
        ...createReservationDto.reservation,
        // @ts-ignore
        client: client._id,
        status: "aanvraag",
        isPaid: false,
      })
      .catch(console.error)) as R.Reservation;

    return {
      client,
      reservation,
    };
  }

  @N.Post("/create-new-user")
  @N.Header("Content-Type", "application/json")
  @A.Public()
  async createNewUserAndPortal(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createUserDto: D.CreatePublicUserDTO,
  ): Promise<U.User> {
    const portalId = await this.portalService.create({
      name: `Portal van ${createUserDto.username}`,
      isActive: true,
    });

    const owner = await this.usersService.create(
      {
        ...createUserDto,
        role: "admin",
        isActive: true,
        // initial portalId
        // @ts-ignore
        portalId: [portalId._id],
      }
    );

    // @ts-ignore
    await this.portalService.putOne(portalId._id, {
        ...portalId,
        // @ts-ignore
        ownerId: owner._id
      }, 
      // @ts-ignore
      portalId._id
    )

    return owner
  }
}
