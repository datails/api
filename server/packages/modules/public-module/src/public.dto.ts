import * as SW from '@nestjs/swagger';
import * as V from "class-validator";
import * as R from "@module/availability-module";
import * as C from "@module/client-module";

export class CreatePublicReservationDTO {
  @SW.ApiProperty({ type: String, required: false })
  @V.IsOptional()
  @V.IsString()
  readonly recaptchaValue?: string;

  @SW.ApiProperty({ type: String, required: false })
  @V.IsOptional()
  @V.IsString()
  readonly "g-recaptcha-response"?: string;

  @SW.ApiProperty({ type: C.CreateClientDto, required: true })
  @V.IsNotEmpty()
  client: C.CreateClientDto;

  @SW.ApiProperty({ type: R.CreateReservationDto, required: true })
  @V.IsNotEmpty()
  reservation: R.CreateReservationDto;
}

export class CreatePublicUserDTO {
  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsEmail()
  readonly email: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly name: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly username: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly password: string;
}
