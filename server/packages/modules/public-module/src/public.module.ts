import { Module, HttpModule } from "@nestjs/common";
import { PropertyModule } from "@module/property-module";
import { PortalModule } from "@module/portal-module";
import { ReservationModule } from "@module/availability-module";
import { UsersModule } from "@module/user-module";
import { ClientModule } from "@module/client-module";
import { EmailModule } from "@module/email-module";
import { PublicController } from "./public.controller";

@Module({
  imports: [
    HttpModule,
    PortalModule,
    EmailModule,
    ClientModule,
    PropertyModule,
    ReservationModule,
    UsersModule,
  ],
  controllers: [PublicController],
})
export class PublicModule {}
