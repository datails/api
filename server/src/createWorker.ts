import { isMainThread } from "worker_threads";
import { NestFactory } from "@nestjs/core";
import { ExpressAdapter } from "@nestjs/platform-express";
import * as express from "express";
import { WorkerModule } from "@module/worker-module";

async function createWorker() {
  if (!isMainThread) {
    const app = await NestFactory.create(
      WorkerModule,
      new ExpressAdapter(express())
    );
    await app.init();
  }
}

void createWorker();
