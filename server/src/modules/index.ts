import { Module, forwardRef } from "@nestjs/common";
import { MulterModule } from "@nestjs/platform-express";

import { AuthModule } from '@module/auth-module';
import { SwitchModule } from '@module/switch-module';
import { LayoutModule } from '@module/layout-module';
import { MongoModule } from '@module/mongo-module';
import { UsersModule } from '@module/user-module';
import { ConfigModule } from '@module/config-module';
import { PropertyModule } from '@module/property-module';
import { ReservationModule } from '@module/availability-module';
import { ClientModule } from '@module/client-module';
import { PortalModule } from '@module/portal-module';
import { PublicModule } from '@module/public-module';

@Module({
  imports: [
    SwitchModule.forRoot(),
    forwardRef(() => MongoModule),
    forwardRef(() => ConfigModule),
    forwardRef(() => AuthModule),
    forwardRef(() => UsersModule),
    forwardRef(() => PropertyModule),
    forwardRef(() => ReservationModule),
    forwardRef(() => ClientModule),
    forwardRef(() => PublicModule),
    forwardRef(() => PortalModule),
    forwardRef(() => LayoutModule),
    MulterModule.register(),
  ],
})
export class AppModule {}
