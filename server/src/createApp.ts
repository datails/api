import "dotenv/config";

import { NestFactory } from "@nestjs/core";
import { ValidationPipe, INestApplication } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { ExpressAdapter } from "@nestjs/platform-express";
import * as S from '@nestjs/swagger';
import * as express from "express";

import { WrapInterceptor } from "@module/common";
import { AppModule } from "./modules";

function createSwagger(
  app: INestApplication,
  apiPrefix: string,
) {
  const swaggerOptions = new S.DocumentBuilder()
    .setTitle('VakantieVorden API')
    .setDescription('API to fetch property data.')
    .setVersion('1.0')
    .build();

  const document = S.SwaggerModule.createDocument(app, swaggerOptions);

  S.SwaggerModule.setup(`${apiPrefix}/docs`, app, document);
}

export async function createApp() {
  const server = express();

  const app = await NestFactory.create(AppModule, new ExpressAdapter(server));

  const configService = app.get<ConfigService>(ConfigService);

  const prefix = configService.get<string>("app.prefix")

  app.setGlobalPrefix(prefix);

  app.useGlobalPipes(new ValidationPipe());

  app.useGlobalInterceptors(new WrapInterceptor());

  app.enableCors();

  createSwagger(app, prefix);

  await app.listen(3000);
}
