const fs = require('fs');
const path = require('path');

const {
  NODE_ENV,
  SERVER_IMAGE_NAME,
  CI_PROJECT_NAME,
  CI_COMMIT_BRANCH,
  FRONTEND_IMAGE_NAME,

  JWT_SECRET,
  PUBLIC_PATH,
  QUEUE_NAME,
  MONGO_URI,

} = process.env

const DEFAULT_PROVISION_MAP = {
  __RESOURCE_NAME__: `${CI_PROJECT_NAME}-${CI_COMMIT_BRANCH}`,
  __APP_TYPE__: `${CI_PROJECT_NAME}`,
  __LABEL_ID__: `${CI_PROJECT_NAME}-${CI_COMMIT_BRANCH}-${CI_PROJECT_NAME}`,
  __SERVER_IMAGE_NAME__: SERVER_IMAGE_NAME,
  __FRONTEND_IMAGE_NAME__: FRONTEND_IMAGE_NAME,
  __CONTAINER_PORT__: 3000,
  __SVC_PORT__: 80,
  __NODE_ENV__: NODE_ENV,
}

const SPECIFIC_PROVISION_MAP = {
  __PUBLIC_PATH__: PUBLIC_PATH,
  __QUEUE_NAME__: QUEUE_NAME,
  __MONGO_URI__: MONGO_URI,
  __JWT_SECRET__: JWT_SECRET,
}

async function provisionK8sFiles() {
  const DIR = path.join('gitlab', 'k8s');
  const K8S_FILES = await fs.promises.readdir(DIR);

  const MAPPING = {
    ...DEFAULT_PROVISION_MAP,
    ...SPECIFIC_PROVISION_MAP,
  }

  for (const file of K8S_FILES) {
    const filePath = path.join(DIR, file);

    let content = await fs.promises.readFile(filePath, {
      encoding: 'utf-8'
    });

    content = Object.keys(MAPPING).reduce((acc, curr) => {
      return acc.replace(new RegExp(curr, 'g'), MAPPING[curr])
    }, content)

    fs.writeFileSync(filePath, content, 'utf8')
  }
}

provisionK8sFiles()
